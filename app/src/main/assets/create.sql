CREATE TABLE [LoanStatus] (
	[LnStatus] Integer PRIMARY KEY,
	[StName] nvarchar(20) 
);
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (0, 'Current');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (1, 'Closed');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (2, 'Late1');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (3, 'Late2');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (4, 'NIBL1');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (5, 'NIBL2');

CREATE TABLE [OrgCategories] (
	[OrgCategoryID] nchar(2) PRIMARY KEY,
	[OrgCategoryName] nvarchar(255),
	[Status] tinyint
);

CREATE TABLE [Projects] (
	[ProjectCode] nchar(3) PRIMARY KEY,
	[ProjectName] nvarchar(255),
	[OrgCategory] nchar(2),
	[Status] tinyint
);

CREATE TABLE [POList] (
	[CONo] nchar(8) PRIMARY KEY,
	[COName] nvarchar(255),
	[SessionNo] Integer NULL,
	[OpenDate] datetime NULL,
	[OpeningBal] Integer NULL,
	[Password] nvarchar(10) NULL,
	[EMethod] smallint NULL DEFAULT 0,
	[CashInHand] Integer NULL,
	[EnteredBy] nvarchar(10) NULL,
	[SYSDATE] datetime NULL,
	[SYSTIME] datetime NULL,
	[AltCOName] nvarchar(255),
	[BranchCode] nchar(4),
	[BranchName] nvarchar(255),
	[BODate] datetime NULL,
	[BOStatus] Integer
);
DELETE FROM POList;
INSERT INTO POList([CONo], [COName], [SessionNo], [OpenDate], [OpeningBal], [Password], [EMethod], [CashInHand], [EnteredBy], [AltCOName], [BranchCode], [BranchName], [BODate], [BOStatus])
	VALUES('00000000', 'Nobody', 0, '2015-02-03', 0, '', 0, 0, '00000000', 'Nobody', '0000', '', '2015-02-03', 1);  

CREATE TABLE [COList] (
	[CONo] nchar(8) PRIMARY KEY,
	[COName] nvarchar(255),
	[LastPOSyncTime] datetime
);

CREATE TABLE [VOList] (
	[OrgNo] nchar(4) PRIMARY KEY,
	[OrgName] nvarchar(255),
	[OrgCategory] nchar(2),
	[MemSexID] int,
	[SavColcOption] int,
	[LoanColcOption] int,
	[SavColcDate] datetime,
	[LoanColcDate] datetime,
	[TargetDate] datetime,
	[Period] int,
	[CONo] nchar(8),
	[PeriodStart] datetime,
	[PeriodEnd] datetime,
	[TerritoryName] nvarchar(255)
);

CREATE TABLE [LoanProducts] (
	[ProductNo] nchar(3),
	[ProductName] nvarchar(255),
	[IntrRate] real,
	[IntCalcMethod] tinyint,
	[IntRateIntrvlDays] int,
	[IntrFactorLoan] real,
	PRIMARY KEY ([ProductNo]) 
);

CREATE TABLE [SavingsProducts] (
	[ProductNo] nchar(3),
	[ProductName] nvarchar(255),
	PRIMARY KEY ([ProductNo])
);
	
CREATE TABLE [SavingsInterestPolicy] (
	[ProductNo] nchar(3),
	[OrgCategory] nchar(2),
	[ProjectCode] nchar(3),
	[MinSavBalance] int,
	[MaxSavBalance] int,
	[InterestRate] real,
	PRIMARY  KEY ([ProductNo], [OrgCategory], [ProjectCode])
);
		
CREATE TABLE [CMembers] (
	[ProjectCode] nchar(3),
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[MemberName] nvarchar(255),
	[SavBalan] Integer,
	[SavPayable] Integer,
	[CalcIntrAmt] real,
	[TargetAmtSav] Integer,
	[ColcDate] datetime,
	[ReceAmt] Integer DEFAULT 0,
	[AdjAmt] Integer,
	[SB] Integer,
	[SIA] Integer,
	[SavingsRealized] Integer,
	[memSexID] Integer,
	[NationalIDNo] nvarchar(17),
	[PhoneNo] nvarchar(20),
	[WalletNo] nvarchar(20),
	[AdmissionDate] datetime,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [CLoans] (
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] Integer,
	[LoanSlNo] Integer,
	[ProductNo] nchar(3),
	[IntrFactorLoan] real,
	[PrincipalAmt] Integer,
	[SchmCode] nchar(3),
	[InstlAmtLoan] Integer,
	[DisbDate] datetime,
	[LnStatus] Integer,
	[PrincipalDue] Integer,
	[InterestDue] Integer,		
	[TotalDue] Integer,
	[TargetAmtLoan] Integer,
	[TotalReld] Integer,
	[Overdue] Integer,
	[BufferIntrAmt] real,
	[ReceAmt] Integer DEFAULT 0,
	[IAB] Integer,
	[ODB] Integer,
	[TRB] Integer,
	[LB] Integer,
	[PDB] Integer,
	[IDB] Integer,
	[InstlPassed] Integer,
	[OldInterestDue] real,
	[ProductSymbol] nchar(3),
	[LoanRealized] Integer,
	[ColcDate] datetime,
	PRIMARY KEY ([OrgNo], [OrgMemNo], [LoanNo])
);

CREATE TABLE [ClosedLoans] (
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] Integer,
	[LoanSlNo] Integer,
	[ProductNo] nchar(3),
	[ProductSymbol] nchar(3),
	[IntrRate] real,
	[PrincipalAmt] Integer,
	[InstlAmt] Integer,
	[DisbDate] datetime,
	[LnStatus] Integer,
	[TotalReld] Integer,
	[InstlPassed] Integer,
	[LoanTragetAmount] Integer,
	[WriteOffAmount] Integer,
	PRIMARY KEY ([ProjectCode], [LoanNo])
);

CREATE TABLE [Transact] (
	_id INTEGER PRIMARY KEY, 
	[ColcID] int NULL DEFAULT 0,
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[ColcAmt] Integer,
	[SavAdj] Integer,
	[ColcDate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[ColcFor] nchar(1),
	[ProductSymbol] nchar(3),
    [Balance] Integer DEFAULT 0,
    [SMSStatus] Integer DEFAULT 0
);

CREATE TABLE [ActiveUser] (
    [UserId] nvarchar(20),
    [UserName] nvarchar(50),
    [UserPin]  nvarchar(20),
    [Token] nvarchar(128),
    [EventId] Integer,
    [DateStart] DateTime,
    [DateEnd] DateTime,
    [EventRole] nchar(1),
    [BranchCode] nchar(4),
    [BranchName] nvarchar(255),
    [LastDownload] DateTime

);
INSERT INTO [ActiveUser] ( [UserId], [UserName], [UserPin], [Token], [EventId], [DateStart], [DateEnd], [EventRole], [BranchCode], [BranchName], [LastDownload])
    VALUES ('', '', '', '', 0, '1900-01-01 00:00:00', '1900-01-01 00:00:00', '0', '','', '2018-01-01 00:00:00');

CREATE TABLE [Respondents] (
    [Id] integer DEFAULT 0,
    [EventId] Integer,
    [SectionId] integer,
   	[SubSectionId] nvarchar(20),
    [OrgNo] nvarchar(4),
    [OrgMemNo] nvarchar(6),
    [MemberName] nvarchar(255),
    [AdmissionDate] datetime,
    [LoanNo] integer DEFAULT 0,
    [DisbDate] datetime,
    [Amount] integer DEFAULT 0,
   	[CONo] nchar(8),
   	[COName] nvarchar(255),
   	[MonitorNo] Integer DEFAULT 0,
    [Status] nchar(1) DEFAULT 'N',
    [Updated_At] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
    PRIMARY KEY ([EventId], [SectionId], [SubSectionId], [OrgNo], [OrgMemNo])
);

CREATE TABLE [SurveyData] (
    [Id] integer NOT NULL DEFAULT 0,
    [EventId] Integer,
    [SectionId] integer,
    [SubSectionId] nvarchar(20) DEFAULT '',
    [OrgNo] nvarchar(4),
    [OrgMemNo] nvarchar(6),
    [Question] integer DEFAULT 0,
    [Answer] integer DEFAULT 0,
    [Score] integer DEFAULT 0,
    [Remarks] nvarchar(255),
    [MonitorNo] Integer DEFAULT 0,
    [Status] nchar(1) DEFAULT 'N',
    [Updated_At] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
    [Longi] Double,
    [Lati] Double,
    PRIMARY KEY ([EventId], [SectionId], [SubSectionId], [OrgNo], [OrgMemNo], [Question], [MonitorNo])
);