package net.qsoft.monitor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.monitor.data.ActiveUser;
import net.qsoft.monitor.data.DAO;


public class MainActivity extends SSActivity implements LocationListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    LocationManager locationManager;
    Menu mainMenu = null;
    private ActiveUser usr;

    TextView collLable;
    TextView userLabel;
    TextView branchLabel;
    TextView dateLabel;
    TextView roleLabel;

    RelativeLayout show_event_layout;
    int flag=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //  setSupportActionBar(toolbar);

        collLable = (TextView) findViewById(R.id.collLabel);
        userLabel = (TextView) findViewById(R.id.userLabel);
        branchLabel = (TextView) findViewById(R.id.branchLabel);
        dateLabel = (TextView) findViewById(R.id.dateLabel);
        roleLabel = (TextView) findViewById(R.id.roleLabel);

        show_event_layout = (RelativeLayout) findViewById(R.id.show_event_layout);

        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        usr = ActiveUser.getInstance();
        if (usr.getUserId() == null || usr.getUserId().trim().length() == 0)
            startActivityForResult(new Intent(getApplicationContext(), LoginActivity.class), 1);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(flag==1)
            refreshView();
    }


    private void refreshView() {
        usr = ActiveUser.getInstance();

        if (usr.IsLoggedIn()) {
            collLable.setText("Device assigned to:");
            userLabel.setText(usr.getUserName());
            userLabel.setVisibility(View.VISIBLE);
            show_event_layout.setVisibility(View.VISIBLE);
            branchLabel.setText("Branch: " + usr.getBranchCode() + " - " + usr.getBranchName());
            dateLabel.setText("Date: " + P8.FormatDate(usr.getDateStart(), "dd-MMM-yyyy") + " to " + P8.FormatDate(usr.getDateEnd(), "dd-MMM-yyyy"));
            roleLabel.setText("User role: " + usr.getEventRoleName());
        } else {
            userLabel.setVisibility(View.INVISIBLE);
            show_event_layout.setVisibility(View.INVISIBLE);
            collLable.setText(R.string.coll_not_assigned);
        }
        if (mainMenu != null)
            mainMenu.getItem(0).setEnabled(usr.IsLoggedIn());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1: {
                if (resultCode == Activity.RESULT_OK) {
                    SyncData();
                    refreshView();
                    flag=1;
                } else if (resultCode == Activity.RESULT_FIRST_USER) {
                    flag=0;
                    startActivityForResult(new Intent(getApplicationContext(), MonitorChangeOption.class), 1);
                }
            }
            break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.changeUser) {
            Intent intent = new Intent(getApplicationContext(), ChangeUserActivity.class);
            startActivity(intent);
        }
        if (id == R.id.logout) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        super.onPrepareOptionsMenu(menu);
        mainMenu = menu;
        return true;
    }


    public void SyncData() {
        DAO da = new DAO(this);
        da.open();
        da.clearOtherEventsData(usr.getEventId());
        da.close();

    }

    public void getLocation() {
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void CheckPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        double tvLongi = location.getLongitude();
        double tvLati = location.getLatitude();


    }


    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider!" + provider,
                Toast.LENGTH_SHORT).show();
    }
}
