package net.qsoft.monitor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import net.qsoft.monitor.NetworkService.ErrorDialog;
import net.qsoft.monitor.NetworkService.VolleyCustomRequest;
import net.qsoft.monitor.data.ActiveUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.util.Log.d;

public class ChangeMonitor extends AppCompatActivity {
    private static final String TAG = ChangeMonitor.class.getSimpleName();

    private Spinner monitorSpinner;
    private static final String SPINNER_URL = "http://103.43.93.162/mnw/monitor_selection";
    private static final String hit_url = "http://103.43.93.162/mnw/changemonitor";
    private String selectMonitor, monitor_id, pos;
    Button submit;
    ArrayList<String> monitorList;
    ProgressDialog progressDialog;
    ErrorDialog errorDialog;
    String evenRoll, token, branchcode;
    int evenId;
    int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.monitor_select_layout);
        POExistsALert();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void dataSendToServer() {
        // progressDialog.showProgress();

        ActiveUser usr = ActiveUser.getInstance();
        token = usr.getToken();
        branchcode = usr.getBranchCode();
        evenId = usr.getEventId();
        HashMap<String, String> params = new HashMap<>();
        params.put("token", token);
        params.put("evenRoll", evenRoll);
        params.put("branchcode", branchcode);
        params.put("evenId", String.valueOf(evenId));

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hit_url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            d("Message", status);

                            if (status.equals("success")) {
                                // progressDialog.hideProgress();
                                Toast.makeText(getApplicationContext(),
                                        response.getString("message"), Toast.LENGTH_SHORT).show();
                                ActiveUser usr = ActiveUser.getInstance();
                                usr.setEventRole(response.getInt("eventrole"));
                                usr.Save();
                                setResult(Activity.RESULT_OK, null);
                                finish();

                            } else {
                                // progressDialog.hideProgress();
                                // errorDialog.showDialog("Error!", "Try Again Later");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            // progressDialog.hideProgress();
                            // errorDialog.showDialog("Error!", "Try Again Later.");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // progressDialog.hideProgress();
                        d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            // errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                        } else if (volleyError instanceof ServerError) {
                            // errorDialog.showDialog("Server Error!", "Server Not Found,Try Again Later!");

                        } else if (volleyError instanceof AuthFailureError) {
                            // errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");

                        } else if (volleyError instanceof ParseError) {

                            // errorDialog.showDialog("Parsing Error!", "Parsing Error, Try Again Later.");
                        } else if (volleyError instanceof NoConnectionError) {
                            // errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                        } else if (volleyError instanceof TimeoutError) {
                            // errorDialog.showDialog("Request Timeout!", "Please Check Your Internet Connection");
                        }

                    }
                });

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
        //progressDialog.hideProgress();
    }


    public void POExistsALert() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Are you sure you want to change this Monitor?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ActiveUser usr = ActiveUser.getInstance();
                        evenRoll = usr.getEventRole().toString();
                        if (evenRoll == "1") {

                            POExistsALert1();
                        } else if (evenRoll == "2") {

                            POExistsALert2();

                        }

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActiveUser usr = ActiveUser.getInstance();
                        evenRoll = usr.getEventRole().toString();
                        dataSendToServer();
                    }
                }).show();
    }

    public void POExistsALert1() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention please! ")
                .setMessage("Now your role is:-01. Are you sure you want to change from role:-01 to role:-02?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        evenRoll = "2";
                        dataSendToServer();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        POExistsALert();

                    }
                }).show();
    }

    public void POExistsALert2() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention Please !")
                .setMessage("Now your role is:-02. Are you sure you want to change from role:-02 to role:-01?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        evenRoll = "1";
                        dataSendToServer();

                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        POExistsALert();

                    }
                }).show();
    }

}
