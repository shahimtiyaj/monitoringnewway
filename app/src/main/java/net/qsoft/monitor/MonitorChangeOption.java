package net.qsoft.monitor;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import net.qsoft.monitor.NetworkService.ErrorDialog;
import net.qsoft.monitor.NetworkService.VolleyCustomRequest;
import net.qsoft.monitor.data.ActiveUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static android.util.Log.d;

/**
 * Created by QSPA10 on 3/19/2018.
 */

public class MonitorChangeOption extends AppCompatActivity {
    private static final String TAG = MonitorChangeOption.class.getSimpleName();

    private static final String hit_url = "http://103.43.93.162/mnw/changemonitor";
    Button btnOK, btnCencel;
    net.qsoft.monitor.NetworkService.ProgressDialog progressDialog;
    ErrorDialog errorDialog;
    String evenRoll, token, branchcode;
    int evenId;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.monitor_select_layout);

        progressDialog = new net.qsoft.monitor.NetworkService.ProgressDialog(MonitorChangeOption.this);
        errorDialog = new ErrorDialog(MonitorChangeOption.this);

        btnOK = (Button) findViewById(R.id.btn_ok);
        btnCencel = (Button) findViewById(R.id.btn_cencel);
        txt = (TextView) findViewById(R.id.txt);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {

                ActiveUser usr = ActiveUser.getInstance();
                evenRoll = usr.getEventRole().toString();
                if (evenRoll.equals("1")) {
                    txt.setText("Now your role is:-01. Are you sure you want to change from role:-01 to role:-02?");
                    evenRoll = "2";
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dataSendToServer();
                        }
                    });

                    btnCencel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            startActivity(getIntent());
                        }
                    });

                } else if (evenRoll.equals("2")) {
                    txt.setText("Now your role is:-02. Are you sure you want to change from role:-02 to role:-01?");
                    evenRoll = "1";
                    btnOK.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dataSendToServer();
                        }
                    });

                    btnCencel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(getIntent());
                        }
                    });

                }
            }
        });

        btnCencel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActiveUser usr = ActiveUser.getInstance();
                evenRoll = usr.getEventRole().toString();
                dataSendToServer();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void dataSendToServer() {

        progressDialog.showProgress();

        ActiveUser usr = ActiveUser.getInstance();
        token = usr.getToken();
        branchcode = usr.getBranchCode();
        evenId = usr.getEventId();
        HashMap<String, String> params = new HashMap<>();
        params.put("token", token);
        params.put("evenRoll", evenRoll);
        params.put("branchcode", branchcode);
        params.put("evenId", String.valueOf(evenId));

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hit_url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String status = response.getString("status");
                            d("Message", status);

                            if (status.equals("success")) {
                                progressDialog.hideProgress();
                                Toast.makeText(getApplicationContext(),
                                        response.getString("message"), Toast.LENGTH_SHORT).show();
                                ActiveUser usr = ActiveUser.getInstance();
                                usr.setEventRole(response.getInt("eventrole"));
                                usr.Save();
                                setResult(Activity.RESULT_OK, null);
                                finish();

                            } else {
                                progressDialog.hideProgress();
                                errorDialog.showDialog("Error!", "Try Again Later");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.hideProgress();
                            errorDialog.showDialog("Error!", "Try Again Later.");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.hideProgress();
                        d(TAG, "Error: " + volleyError.getMessage());

                        if (volleyError instanceof NetworkError) {
                            errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                        } else if (volleyError instanceof ServerError) {
                            errorDialog.showDialog("Server Error!", "Server Not Found,Try Again Later!");

                        } else if (volleyError instanceof AuthFailureError) {
                            errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");

                        } else if (volleyError instanceof ParseError) {

                            errorDialog.showDialog("Parsing Error!", "Parsing Error, Try Again Later.");
                        } else if (volleyError instanceof NoConnectionError) {
                            errorDialog.showDialog("No Internet!", "Enable Moblie Data or WIFI");
                        } else if (volleyError instanceof TimeoutError) {
                            errorDialog.showDialog("Request Timeout!", "Please Check Your Internet Connection");
                        }


                    }
                });

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
        progressDialog.hideProgress();
    }

}