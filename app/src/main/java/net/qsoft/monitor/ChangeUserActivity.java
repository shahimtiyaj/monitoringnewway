package net.qsoft.monitor;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import net.qsoft.monitor.NetworkService.VolleyCustomRequest;
import net.qsoft.monitor.data.ActiveUser;
import net.qsoft.monitor.data.GetData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import dmax.dialog.SpotsDialog;

/**
 * Created by QSPA10 on 3/20/2018.
 */

public class ChangeUserActivity extends SSActivity {

    protected static final String TAG = "ChangeUserActivity";
    // UI references----------
    private EditText OldPass;
    private EditText NewPass;
    String token, userPin;
    Button ChangePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);
        initView();
    }

    public void ChangeUser() {

        new SpotsDialog(this, R.style.Custom).show();

        String change_pass_url = "http://103.43.93.162/mnw/changepassword";

        String oldPass = OldPass.getText().toString().trim();
        String newPass = NewPass.getText().toString().trim();
        ActiveUser usr = ActiveUser.getInstance();
        userPin = usr.getUserPin();
        token = usr.getToken();

        HashMap<String, String> params = new HashMap<>();
        params.put("oldPass", oldPass); //Items - Item 2  new password
        params.put("newPass", newPass); //Items - Item 2  new password
        params.put("userPin", userPin);
        params.put("token", token); //Items - Item 1 - resetcode
        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, change_pass_url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Register Response password change: " + response.toString());

                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                new SpotsDialog(getApplicationContext(), R.style.Custom).dismiss();
                                Toast.makeText(getApplicationContext(),
                                        response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            new SpotsDialog(getApplicationContext(), R.style.Custom).dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // TODO Auto-generated method stub
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            message = volleyError.toString();
                        }
                        P8.ShowError(ChangeUserActivity.this, message);
                    }
                });

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
        new SpotsDialog(getApplicationContext(), R.style.Custom).dismiss();

    }

    public void initView() {
        OldPass = (EditText) findViewById(R.id.email);
        NewPass = (EditText) findViewById(R.id.password);
        OldPass.setHint("Old Password");
        NewPass.setHint("New Password");
        TextView txt = (TextView) findViewById(R.id.login_title);
        txt.setText("Change Password");
        OldPass.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        ChangePass = (Button) findViewById(R.id.email_sign_in_button);
        ChangePass.setText("Submit");
        ChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChangeUser();
            }
        });
    }

}