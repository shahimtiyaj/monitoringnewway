package net.qsoft.monitor.data;

public class OrgCategory {
	String _OrgCategoryID;
	String _OrgCategoryName;
	Integer _Status;

	/**
	 * @param _OrgCategoryID
	 * @param _OrgCategoryName
	 * @param _Status
	 */
	public OrgCategory(String _OrgCategoryID, String _OrgCategoryName,
			Integer _Status) {
		super();
		this._OrgCategoryID = _OrgCategoryID;
		this._OrgCategoryName = _OrgCategoryName;
		this._Status = _Status;
	}

	/**
	 * @return the _OrgCategoryID
	 */
	public final String get_OrgCategoryID() {
		return _OrgCategoryID;
	}
	/**
	 * @return the _OrgCategoryName
	 */
	public final String get_OrgCategoryName() {
		return _OrgCategoryName;
	}
	/**
	 * @return the _Status
	 */
	public final Integer get_Status() {
		return _Status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (get_OrgCategoryID() != null ? get_OrgCategoryID() : "") + " - "
				+ (get_OrgCategoryName() != null ? get_OrgCategoryName() : "");
	}

}
