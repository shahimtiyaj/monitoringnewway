package net.qsoft.monitor.data;


import net.qsoft.monitor.P8;

import java.util.Date;

/**
 * Created by zferdaus on 9/12/2017.
 */

public class Respondent {
    private Integer _Id=0;
    private Integer _EventId=0;
    private Integer _SectionId=0;
    private String _SubSectionId="";
    private String _OrgNo;
    private String _OrgMemNo;
    private String _MemberName;
    private Date _AdmissionDate;
    private Integer _LoanNo;
    private Date _DisbDate;
    private Integer _Amount;
    private String _CONo;
    private String _COName;
    private String _Status;     // N - New, U - Update, D - Delete. Used in server
    private Date _Updated_At;
    private Integer _MonitorNo;

    public  Boolean hasServeyData = false;
    public  Boolean isSelected = false;
    public  Boolean isPreSelected = false;

    public static Respondent lastRespondent=null;

    public Respondent() {
        _Id = 0;
        _EventId = 0;
        _SectionId = 0;
        _SubSectionId = "";
        _OrgNo = "";
        _OrgMemNo = "";
        _MemberName = "";
        _AdmissionDate = P8.MinDate();
        _LoanNo = 0;
        _DisbDate = P8.MinDate();
        _Amount = 0;
        _CONo = "";
        _COName = "";
        _Status = "N";
        _MonitorNo = 0;
    }

    public Respondent(Integer _Id) {
        this._Id = _Id;
    }

    public Integer getId() {
        if(_Id == null)
           _Id = 0;
        return _Id;
    }

    public void setId(Integer _Id) {
        this._Id = _Id;
    }

    public Integer getEventId() {
        return _EventId;
    }

    public void setEventId(Integer _EventId) {
        this._EventId = _EventId;
    }

    public Integer getSectionId() {
        return _SectionId;
    }

    public void setSectionId(Integer _SectionId) {
        this._SectionId = _SectionId;
    }

    public String getSubSectionId() {
        return _SubSectionId;
    }

    public void setSubSectionId(String _SubSectionId) {
        this._SubSectionId = _SubSectionId;
    }

    public String getOrgNo() {
        return _OrgNo;
    }

    public void setOrgNo(String _OrgNo) {
        this._OrgNo = _OrgNo;
    }

    public String getOrgMemNo() {
        return _OrgMemNo;
    }

    public void setOrgMemNo(String _OrgMemNo) {
        this._OrgMemNo = _OrgMemNo;
    }

    public String getMemberName() {
        return _MemberName;
    }

    public void setMemberName(String _MemberName) {
        this._MemberName = _MemberName;
    }

    public Date getAdmissionDate() {
        return _AdmissionDate;
    }

    public void setAdmissionDate(Date _AdmissionDate) {
        this._AdmissionDate = _AdmissionDate;
    }

    public void setAdmissionDate(String _AdmissionDate) {
        if(_AdmissionDate == null)
            this._AdmissionDate = P8.MinDate();
        else
            this._AdmissionDate = P8.ConvertStringToDate(_AdmissionDate, "yyyy-MM-dd");
    }

    public Integer getLoanNo() {
        return _LoanNo;
    }

    public void setLoanNo(Integer _LoanNo) {
        this._LoanNo = _LoanNo;
    }

    public Date getDisbDate() {
        return _DisbDate;
    }

    public void setDisbDate(Date _DisbDate) {
        this._DisbDate = _DisbDate;
    }

    public void setDisbDate(String _DisbDate) {
        if(_DisbDate == null)
            this._DisbDate = P8.MinDate();
        else
            this._DisbDate = P8.ConvertStringToDate(_DisbDate, "yyyy-MM-dd");
    }

    public Integer getAmount() {
        return _Amount;
    }

    public void setAmount(Integer _Amount) {
        this._Amount = _Amount;
    }

    public String getCONo() {
        return _CONo;
    }

    public void setCONo(String _CONo) {
        this._CONo = _CONo;
    }

    public String getCOName() {
        return _COName;
    }

    public void setCOName(String _COName) {
        this._COName = _COName;
    }

    public String getStatus() {
        return _Status;
    }

    public void setStatus(String _Status) {
        this._Status = _Status;
    }

    public Date getUpdated_At() {
        return _Updated_At;
    }

    public void setUpdated_At(Date _Updated_At) {
        this._Updated_At = _Updated_At;
    }
    public void setUpdated_At(String _Updated_At) {
        this._Updated_At = P8.ConvertStringToDate(_Updated_At, "yyyy-MM-dd hh:mm:ss");
    }

    public Integer getMonitorNo() {
        return _MonitorNo;
    }

    public void setMonitorNo(Integer _MonitorNo) {
        this._MonitorNo = _MonitorNo;
    }

}
