package net.qsoft.monitor.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import net.qsoft.monitor.CloudRequest;
import net.qsoft.monitor.NetworkService.VolleyCustomRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import static net.qsoft.monitor.data.DBHelper.TBL_RESPONDENTS;
import static net.qsoft.monitor.data.DBHelper.TBL_SURVEYDATA;

/**
 * Created by QSPA10 on 3/20/2018.
 */

public class GetData {

    @SuppressLint("StaticFieldLeak")
    private static GetData mInstance = null;
    @SuppressLint("StaticFieldLeak")
    private static Context mCtx;
    private static String token, evenId, lastDownloadTime;

    private GetData() {
    }

    public static synchronized GetData getInstance() {
        if (mInstance == null) {
            mInstance = new GetData();
        }
        return mInstance;
    }

    public void dataGetFromServer(Context context) {
        mCtx = context;

        String hit_url = "http://103.43.93.162/mnw/download";
        ActiveUser usr = ActiveUser.getInstance();
        token = usr.getToken();
        evenId = String.valueOf(usr.getEventId());
        lastDownloadTime = String.valueOf(usr.getLastDownload());
        HashMap<String, String> params = new HashMap<>();
        params.put("token", token);
        params.put("eventid", evenId);
        params.put("lastDownload", lastDownloadTime);

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hit_url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            Log.d("response", "Size: " + response);

                            if (status.equals("success")) {

                                JSONObject jObj = response.getJSONObject("data");
                                JSONArray getSurveyData = jObj.getJSONArray("servey_data");

                                for (int i = 0; i < getSurveyData.length(); i++) {

                                    JSONObject c = getSurveyData.getJSONObject(i);

                                    DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_SURVEYDATA + "(Id, EventId, SectionId," +
                                                    " SubSectionId, OrgNo, OrgMemNo, Question, Answer, Score, Remarks," +
                                                    " MonitorNo, Status, Updated_At, Longi, Lati) " +
                                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                            new String[]{c.getString("id"),
                                                    c.getString("eventid"),
                                                    c.getString("sec_no"),
                                                    c.getString("sub_sec_id"),
                                                    c.getString("orgno"),
                                                    c.getString("orgmemno"),
                                                    c.getString("question"),
                                                    c.getString("answer"),
                                                    c.getString("score"),
                                                    c.getString("remarks"),
                                                    c.getString("monitorno"),
                                                    "",
                                                    c.getString("time"),
                                                    c.getString("longi"),
                                                    c.getString("lati")});

                                    Toast.makeText(mCtx, "Survey Data Loding...", Toast.LENGTH_LONG).show();

                                }

                                JSONArray getResponDatnt = jObj.getJSONArray("respondents");

                                for (int i = 0; i < getResponDatnt.length(); i++) {

                                    JSONObject c = getResponDatnt.getJSONObject(i);

                                    DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_RESPONDENTS + "(Id, EventId, SectionId, " +
                                                    "SubSectionId, OrgNo, OrgMemNo, MemberName, AdmissionDate, LoanNo," +
                                                    " DisbDate, Amount, CONo, COName, MonitorNo, Status, Updated_At) " +
                                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                            new String[]{c.getString("id"),
                                                    c.getString("event_id"),
                                                    c.getString("sec_no"),
                                                    c.getString("sub_sec_id"),
                                                    c.getString("orgno"),
                                                    c.getString("orgmemno"),
                                                    c.getString("MemberName"),
                                                    c.getString("admission_date"),
                                                    c.getString("loanno"),
                                                    c.getString("disbdate"),
                                                    c.getString("amnt"),
                                                    c.getString("cono"),
                                                    c.getString("coname"),
                                                    c.getString("MonitorNo"),
                                                    "",
                                                    c.getString("time")});
                                    Toast.makeText(mCtx, "Respondant Data Loding...", Toast.LENGTH_LONG).show();

                                }

                            } else {
                                Toast.makeText(mCtx, "Try again later!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(mCtx, "Try again later!", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // TODO Auto-generated method stub
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            message = volleyError.toString();
                        }
                        //    P8.ShowError(getApplicationContext(), message);
                    }
                });

        CloudRequest.getInstance(mCtx).addToRequestQueue(postRequest);
    }

    public String LastDownloadTime() {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();
        return dt.format(date);
        // return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(date);
        // return DateFormat.getDateTimeInstance().format(new Date());//latest and standard formt
    }
}
