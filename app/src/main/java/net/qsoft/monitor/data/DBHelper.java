package net.qsoft.monitor.data;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	private static final String TAG = SQLiteOpenHelper.class.getSimpleName();
	 
    final static int DB_VERSION = 1;
    final static String DB_NAME = "mnw.s3db";

    final static String TBL_ACTIVE_USER ="ActiveUser";
    final static String TBL_BRANCH="branch";
    final static String TBL_PROJECTS = "Projects";
    final static String TBL_LOAN_PRODUCTS = "LoanProducts";
    final static String TBL_PO ="POList";
    final static String TBL_COLIST ="COList";
    final static String TBL_VOLIST = "VOList";
    final static String TBL_CMEMBERS = "CMembers";
    final static String TBL_CLOANS = "CLoans";
    final static String TBL_LOANSTATUS = "LoanStatus";
    final static String TBL_WPTRANSACT = "WPTransact";
    final static String TBL_GOOD_LOANS = "GoodLoans";
    final static String TBL_TRANSACT = "Transact";
    public final static String TBL_RESPONDENTS="Respondents";
    public final static String TBL_SURVEYDATA="SurveyData";

    // TBL_PROJECTS field names
    final static String FLD_PROJECT_CODE = "[ProjectCode]";
    final static String FLD_PROJECT_NAME = "[ProjectName]";
    
    // TBL_LOAN_PRODUCTS field names
    final static String FLD_PRODUCT_NO = "[ProductNo]";
    final static String FLD_PRODUCT_NAME = "[ProductName]";
    final static String FLD_INTEREST_RATE = "[IntRate]";
    final static String FLD_INTEREST_CALC_METHOD = "[IntCalcMethod]";
    final static String FLD_INT_RATE_INTERVAL_DAYS = "[IntRateIntrvlDays]";
    final static String FLD_LOAN_INT_CALC_FACTOR = "[IntrFactorLoan]";

    // TBL_COLIST field name
    final public static String FLD_CONO = "[CONo]";
    final public static String FLD_CONAME = "[CONAME]";
    final public static String FLD_COLastyncTime = "[LastPOSyncTime]";

    // TBL_VOLIST field names
    final public static String FLD_ORG_NO = "[OrgNo]";
    final public static String FLD_ORG_NAME = "[OrgName]";
    final public static String FLD_TARGETDATE = "[TargetDate]";
    final public static String FLD_PERIOD_START = "[PeriodStart]";
    final public static String FLD_PERIOD_END = "[PeriodEnd]";

    // TBL_CMEMBERS field names
    final public static String FLD_ORG_MEM_NO = "[OrgMemNo]";
    final public static String FLD_ORG_MEM_NAME = "[MemberName]";
    final public static String FLD_TARGET_SAVINGS_AMT = "[TargetAmtSav]";
    final public static String FLD_ADMISSION_DATE= "[AdmissionDate]";
    
    // TBL_CLOANS field names
    public final static String FLD_LOAN_NO = "[LoanNo]"; 
    public final static String FLD_LOAN_SL_NO = "[LoanSlNo]";
    public final static String FLD_PRINCIPAL_AMT = "[PrincipalAmt]";
    public final static String FLD_LOAN_INSTL_AMT = "[InstlAmtLoan]";
    public final static String FLD_DISBURSEMENT_DATE = "[DisbDate]";
    final static String FLD_LOAN_STATUS = "[LnStatus]";
    final static String FLD_PRINCIAL_DUE = "[PrincipalDue]";
    final static String FLD_INTEREST_DUE = "[InterestDue]";
    final static String FLD_TOTAL_DUE = "[TotalDue]";
    public final static String FLD_TARGET_LOAN_AMT = "[TargetAmtLoan]";
    final static String FLD_TOTAL_REALISED_AMT = "[TotalReld]";
    final static String FLD_OVERDUE_AMT = "[Overdue]";
    final static String FLD_BUFFER_INT_AMT = "[BufferIntrAmt]";
    public final static String FLD_RECEIVED_AMT = "[ReceAmt]";
    final static String FLD_TARGET_AMT_LOAN_DUE = "[TALB]";
    final static String FLD_OVERDUE_BAL = "[ODB]";
    public final static String FLD_LOAN_BAL = "[LB]";

    // TBL_LOANSTATUS
    public final static String FLD_LOAN_STATUS_NAME = "[StName]";
    // TBL_TRASACT
    public final static String FLD_COLC_ID = "[ColcID]";
    public final static String FLD_COLC_AMT = "[ColcAmt]";
    public final static String FLD_COLC_DATE = "[ColcDate]";
    public final static String FLD_COLC_FOR = "[ColcFor]";
    


    Context context;
     
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        // Store the context for later use
        this.context = context;
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		try {
			executeSQLScript(db, "create.sql");
		} catch (SQLException e) {
		} catch (IOException e) {
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		try {
			if (newVersion > oldVersion) {
			    switch (oldVersion) {
			        case 1:
			            executeSQLScript(db, "update_v2.sql");
//                        Log.d(TAG, "case 1: old " + oldVersion + " new " + newVersion);
			        case 2:
			            executeSQLScript(db, "update_v3.sql");
//                        Log.d(TAG, "case 2: old " + oldVersion + " new " + newVersion);
			    }
			}
		} catch (SQLException e) {
//            Log.d(TAG, "SQL Exception: " + e.getMessage());
		} catch (IOException e) {
//            Log.d(TAG, "IO Exception: " + e.getMessage());
		}
	}
	
	private void executeSQLScript(SQLiteDatabase database, String dbname) throws IOException, SQLException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte buf[] = new byte[1024];
		int len;
		AssetManager assetManager = context.getAssets();
		InputStream inputStream = null;
		         
		try {
			inputStream = assetManager.open(dbname);
		    while ((len = inputStream.read(buf)) != -1) {
		    	outputStream.write(buf, 0, len);
		    }
		    outputStream.close();
		    inputStream.close();
		             
		    String[] createScript = outputStream.toString().split(";");
		    for (int i = 0; i < createScript.length; i++) {
		    	String sqlStatement = createScript[i].trim();
		        // TODO You may want to parse out comments here
		        if (sqlStatement.length() > 0) {
		        	database.execSQL(sqlStatement + ";");
		        }
		    }
		} 
		catch (IOException e){
		        // TODO Handle Script Failed to Load
			Log.e(TAG, e.toString(), e);
			throw e;
		} 
		catch (SQLException e) {
		        // TODO Handle Script Failed to Execute
			Log.e(TAG, e.toString(), e);
			throw e;
		}
	}

}
