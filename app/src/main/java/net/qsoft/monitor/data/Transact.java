package net.qsoft.monitor.data;

import java.util.Date;

public class Transact {

	Integer _id; 
	Integer _ColcID;
	String _OrgNo;
	String _OrgMemNo;
	String _ProjectCode;
	Integer _LoanNo;
	Integer _ColcAmt;
	Integer _SavAdj;
	Date _ColcDate;
	String _ColcFor;
	String _ProductSymbol;
	Integer _Balance;
	Integer _SMSStatus;

	public Transact() {
		super();
	}

	/**
	 * @param _id
	 * @param _ColcID
	 * @param _OrgNo
	 * @param _OrgMemNo
	 * @param _ProjectCode
	 * @param _LoanNo
	 * @param _ColcAmt
	 * @param _SavAdj
	 * @param _ColcDate
	 * @param _ColcFor
	 */
	public Transact(Integer _id, Integer _ColcID, String _OrgNo,
			String _OrgMemNo, String _ProjectCode, Integer _LoanNo,
			Integer _ColcAmt, Integer _SavAdj, Date _ColcDate, String _ColcFor,
			String _ProductSymbol, Integer _Balance, Integer _SMSStatus) {
		super();
		this._id = _id;
		this._ColcID = _ColcID;
		this._OrgNo = _OrgNo;
		this._OrgMemNo = _OrgMemNo;
		this._ProjectCode = _ProjectCode;
		this._LoanNo = _LoanNo;
		this._ColcAmt = _ColcAmt;
		this._SavAdj = _SavAdj;
		this._ColcDate = _ColcDate;
		this._ColcFor = _ColcFor;
		this._ProductSymbol = _ProductSymbol;
		this._Balance = _Balance;
		this._SMSStatus = _SMSStatus;

	}

	public String get_ProductSymbol() {
		return _ProductSymbol;
	}

	public Integer get_Balance() {
		return _Balance;
	}

	public Integer get_SMSStatus() {
		return _SMSStatus;
	}

	/**
	 * @return the _id
	 */
	public final Integer get_id() {
		return _id;
	}

	/**
	 * @return the _ColcID
	 */
	public final Integer get_ColcID() {
		return _ColcID;
	}

	/**
	 * @return the _OrgNo
	 */
	public final String get_OrgNo() {
		return _OrgNo;
	}

	/**
	 * @return the _OrgMemNo
	 */
	public final String get_OrgMemNo() {
		return _OrgMemNo;
	}

	/**
	 * @return the _ProjectCode
	 */
	public final String get_ProjectCode() {
		return _ProjectCode;
	}

	/**
	 * @return the _LoanNo
	 */
	public final Integer get_LoanNo() {
		return _LoanNo;
	}

	/**
	 * @return the _ColcAmt
	 */
	public final Integer get_ColcAmt() {
		return _ColcAmt;
	}

	/**
	 * @return the _SavAdj
	 */
	public final Integer get_SavAdj() {
		return _SavAdj;
	}

	/**
	 * @return the _ColcDate
	 */
	public final Date get_ColcDate() {
		return _ColcDate;
	}

	/**
	 * @return the _ColcFor
	 */
	public final String get_ColcFor() {
		return _ColcFor;
	}

}
