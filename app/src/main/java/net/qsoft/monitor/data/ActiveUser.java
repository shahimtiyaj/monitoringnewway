package net.qsoft.monitor.data;


import net.qsoft.monitor.App;
import net.qsoft.monitor.P8;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by zferdaus on 9/7/2017.
 */

public class ActiveUser {
    String UserId;
    String UserName;
    String UserPin;
    String Token;
    Integer EventId;
    String BranchCode;
    String BranchName;
    Date DateStart;
    Date DateEnd;
    Integer EventRole;


    Date LastDownload;


    private static ActiveUser mInstance = null;

    private ActiveUser() {
        DAO da = new DAO(App.getContext());
        da.open();
        da.getActiveUser(this);
        da.close();
    }

    public static synchronized ActiveUser getInstance() {
        if (mInstance == null) {
            mInstance = new ActiveUser();
        }
        return mInstance;
    }

    public boolean IsLoggedIn() {
        return (getUserId().length() > 0);
    }

    public boolean isActiveEvent() {
        long toDay = P8.ToDay().getTime();
        return (toDay >= getDateStart().getTime() && toDay <= getDateEnd().getTime());
    }

    public Date getDataEndDate() {
        Calendar c = Calendar.getInstance();
        c.setTime(getDateStart());
        c.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
        c.add(Calendar.DAY_OF_YEAR, -7);
        return c.getTime();
    }

    /*
    public ActiveUser(String userId, String userName, String userPin, String token, Integer eventId, String branchcode, String branchname,  Date dateStart, Date dateEnd, Integer eventRole) {
        UserId = userId;
        UserName = userName;
        UserPin = userPin;
        Token = token;
        EventId = eventId;
        BranchCode = branchcode;
        BranchName = branchname;
        DateStart = dateStart;
        DateEnd = dateEnd;
        EventRole = eventRole;
    }
*/
    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserPin() {
        return UserPin;
    }

    public void setUserPin(String userPin) {
        UserPin = userPin;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public Integer getEventId() {
        return EventId;
    }

    public void setEventId(Integer eventId) {
        EventId = eventId;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchcode) {
        String t = ("0000" + branchcode.trim());
        BranchCode = t.substring(t.length() - 4);
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchname) {
        BranchName = branchname;
    }

    public Date getDateStart() {
        if (DateStart == null)
            DateStart = P8.ConvertStringToDate("1900-01-01", "yyyy-MM-dd");
        return DateStart;
    }

    public void setDateStart(String datStart) {
        setDateStart(P8.ConvertStringToDate(datStart, "yyyy-MM-dd"));
    }

    public void setDateStart(Date dateStart) {
        DateStart = dateStart;
    }

    public Date getDateEnd() {
        if (DateEnd == null)
            DateEnd = P8.ConvertStringToDate("1900-01-01", "yyyy-MM-dd");
        return DateEnd;
    }

    public void setDateEnd(String datStr) {
        setDateEnd(P8.ConvertStringToDate(datStr, "yyyy-MM-dd"));
    }

    public Date getLastDownload() {
        return LastDownload;
    }

    public void setLastDownload(Date lastDownload) {
        LastDownload = lastDownload;
    }

    public void setLastDownload(String datStr) {
        setLastDownload(P8.ConvertStringToDate(datStr, "yyyy-MM-dd hh:mm:ss"));
    }

    public void setDateEnd(Date dateEnd) {
        DateEnd = dateEnd;
    }

    public Integer getEventRole() {
        return EventRole;
    }

    public String getEventRoleName() {
        return "Monitor " + getEventRole().toString();
    }

    public void setEventRole(Integer eventRole) {
        EventRole = eventRole;
    }

    public void Save() {
        DAO da = new DAO(App.getContext());
        da.open();
        try {
            da.updateActiveUser(this);

        } catch (Exception e) {

        }
        da.close();
    }
}
