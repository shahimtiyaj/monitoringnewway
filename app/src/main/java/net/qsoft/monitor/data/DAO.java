package net.qsoft.monitor.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.qsoft.monitor.App;
import net.qsoft.monitor.P8;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DAO {
    private static final String TAG = DAO.class.getSimpleName();

    public enum DBoardItem {
        COS, VOS, MEMBERS, BORROWERS, CURBORROWERS, LOANS, CURLOANS, L1LOANS, L2LOANS, N1LOANS, N2LOANS, OUTLOANS, OVERLOANS, SAVE_TARGET, SAVE_YET_COLLECTED, REPAY_TARGET, REPAY_YET_COLLECTED, PO_CASH_IN_HAND
    }

    // Loan status type constans
    public static final int LS_CURRENT = 0;
    public static final int LS_CLOSED = 1;
    public static final int LS_LATE1 = 2;
    public static final int LS_LATE2 = 3;
    public static final int LS_NIBL1 = 4;
    public static final int LS_NIBL2 = 5;

    // Database fields
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public DAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        if (db != null && db.isOpen())
            db.close();
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    public void execSQL(String sql, String[] param) throws SQLException {
        db.execSQL(sql, param);
    }

    public static void executeSQL(String sql, String[] param) {
        DAO da = new DAO(App.getContext());
        da.open();
        try {
            da.execSQL(sql, param);
        } catch (Exception e) {
            throw e;
        } finally {
            da.close();
        }
    }

    public void WriteSyncTime(String dt) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[LastDownload]", dt);
        db.update(DBHelper.TBL_ACTIVE_USER, cv, null, null);
    }

    public String GetSyncTime() {
        String ret = "";
        Cursor curs = null;

        try {
            curs = db
                    .rawQuery("SELECT [LastDownload] FROM " + DBHelper.TBL_ACTIVE_USER, null);
            if (curs.moveToFirst()) {
                ret = curs.getString(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return ret;
    }

    public Branch getBranch() {
        Branch br = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_BRANCH, new String[]{"BranchCode",
                            "BranchName", "BranchOpeningStatus", "BranchOpenDate",
                            "BranchCloseDate", "OpeningDayType"}, null, null, null,
                    null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                br = new Branch(curs.getString(0), curs.getString(1),
                        curs.getInt(2), curs.getString(3), curs.getString(4),
                        curs.getString(5));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return br;
    }

    public ActiveUser getActiveUser(ActiveUser usr) {
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_ACTIVE_USER, new String[]{"UserId",
                            "UserName", "UserPin", "Token",
                            "EventId", "DateStart", "DateEnd", "EventRole", "BranchCode", "BranchName", "LastDownload"}, null, null, null,
                    null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                usr.setUserId(curs.getString(0));
                usr.setUserName(curs.getString(1));
                usr.setUserPin(curs.getString(2));
                usr.setToken(curs.getString(3));
                usr.setEventId(curs.getInt(4));
                usr.setDateStart(curs.getString(5));
                usr.setDateEnd((curs.getString(6)));
                usr.setEventRole(curs.getInt(7));
                usr.setBranchCode(curs.getString(8));
                usr.setBranchName(curs.getString(9));
                usr.setLastDownload(curs.getString(10));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return usr;
    }

    public void updateActiveUser(ActiveUser usr) throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();

            cv.clear();
            cv.put("[UserId]", usr.getUserId());
            cv.put("[UserName]", usr.getUserName());
            cv.put("[UserPin]", usr.getUserPin());
            cv.put("[Token]", usr.getToken());
            cv.put("[EventId]", usr.getEventId());
            cv.put("[DateStart]", P8.FormatDate(usr.getDateStart(), "yyyy-MM-dd hh:mm:ss"));
            cv.put("[DateEnd]", P8.FormatDate(usr.getDateEnd(), "yyyy-MM-dd hh:mm:ss"));
            cv.put("[EventRole]", usr.getEventRole());
            cv.put("[BranchCode]", usr.getBranchCode());
            cv.put("[BranchName]", usr.getBranchName());
            cv.put("[LastDownload]", P8.FormatDate(usr.getLastDownload(), "yyyy-MM-dd hh:mm:ss"));


            db.update(DBHelper.TBL_ACTIVE_USER, cv, null, null);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            //		throw e;

        } finally {
            db.endTransaction();
        }
    }

    public Respondent getRespondent(Integer eventId, Integer sec, String vono, String memNo) {
        return getRespondent(eventId, sec, "", vono, memNo, false);
    }

    public Respondent getRespondent(Integer eventId, Integer sec, String subsec, String vono, String memNo) {
        return getRespondent(eventId, sec, subsec, vono, memNo, false);
    }

    public Respondent getRespondent(Integer eventId, Integer sec, String subsec, String vono, String memNo, boolean checkServey) {
        Respondent rsp = null;
        Cursor curs = null;
        String sql = "SELECT [Id], [EventId], [SectionId], [OrgNo], [OrgMemNo], [MemberName], [AdmissionDate], [LoanNo], [DisbDate], " + "" +
                "[Amount], [CONo], [COName], [Status], [Updated_At], [MonitorNo] " +
                "FROM Respondents " +
                "WHERE [EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=?";

        try {
            curs = db.rawQuery(sql, new String[]{eventId.toString(), sec.toString(), subsec, vono, memNo});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                rsp = new Respondent();
                rsp.setId(curs.getInt(0));
                rsp.setEventId(curs.getInt(1));
                rsp.setSectionId(curs.getInt(2));
                rsp.setOrgNo(curs.getString(3));
                rsp.setOrgMemNo(curs.getString(4));
                rsp.setMemberName(curs.getString(5));
                rsp.setAdmissionDate(curs.getString(6));
                rsp.setLoanNo(curs.getInt(7));
                rsp.setDisbDate(curs.getString(8));
                rsp.setAmount(curs.getInt(9));
                rsp.setCONo(curs.getString(10));
                rsp.setCOName(curs.getString(11));
                rsp.setStatus(curs.getString(12));
                rsp.setUpdated_At(curs.getString(13));
                rsp.setMonitorNo(curs.getInt(14));
                if (checkServey)
                    rsp.hasServeyData = hasServeyData(eventId, sec, subsec, rsp.getOrgNo(), rsp.getOrgMemNo());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return rsp;
    }


    public ArrayList<Respondent> getRespondents(Integer eventId, Integer sec, boolean checkServey) {
        return getRespondents(eventId, sec, "", 0, checkServey);
    }

    public ArrayList<Respondent> getRespondents(Integer eventId, Integer sec, String subsec, boolean checkServey) {
        return getRespondents(eventId, sec, subsec, 0, checkServey);
    }

    public ArrayList<Respondent> getRespondents(Integer eventId, Integer sec, String subsec, Integer monitorNo, boolean checkServey) {
        return getRespondents(eventId, sec, subsec, monitorNo, checkServey, false);
    }

    public ArrayList<Respondent> getRespondents(Integer eventId, Integer sec, String subsec, Integer monitorNo, boolean checkServey, boolean onlyChanged) {
        ArrayList<Respondent> ret = new ArrayList<Respondent>();
        Cursor curs = null;
        String sql = "SELECT [Id], [EventId], [SectionId], [OrgNo], [OrgMemNo], [MemberName], [AdmissionDate], [LoanNo], [DisbDate], " + "" +
                "[Amount], [CONo], [COName], [Status], [Updated_At], [MonitorNo], [SubSectionId] " +
                "FROM Respondents " +
                "WHERE [EventId]=? "; //AND  [Status]<>'D'

        if (sec > 0)
            sql += " AND [SectionId]=" + sec;
        if (subsec.trim().length() > 0)
            sql += " AND [SubSectionId]='" + subsec + "'";

        if (monitorNo != 0)
            sql += " AND [MonitorNo]=" + monitorNo;

        if (onlyChanged) {
            sql += " AND [Status] <> 'S'";
        } else {
            sql += " AND [Status] <> 'D'";
        }

        //
        try {
            curs = db.rawQuery(sql, new String[]{eventId.toString()});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                do {
                    Respondent rsp = new Respondent();
                    rsp.setId(curs.getInt(0));
                    rsp.setEventId(curs.getInt(1));
                    rsp.setSectionId(curs.getInt(2));
                    rsp.setOrgNo(curs.getString(3));
                    rsp.setOrgMemNo(curs.getString(4));
                    rsp.setMemberName(curs.getString(5));
                    rsp.setAdmissionDate(curs.getString(6));
                    rsp.setLoanNo(curs.getInt(7));
                    rsp.setDisbDate(curs.getString(8));
                    rsp.setAmount(curs.getInt(9));
                    rsp.setCONo(curs.getString(10));
                    rsp.setCOName(curs.getString(11));
                    rsp.setStatus(curs.getString(12));
                    rsp.setUpdated_At(curs.getString(13));
                    rsp.setMonitorNo(curs.getInt(14));
                    rsp.setSubSectionId(curs.getString(15));
                    if (checkServey)
                        rsp.hasServeyData = hasServeyData(eventId, sec, subsec, rsp.getOrgNo(), rsp.getOrgMemNo());
                    ret.add(rsp);

                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }


        return ret;
    }

    public boolean hasUploadedServeyData(Integer eventId, Integer sec, String subSec, String vono, String memNo) {
        String sql = "SELECT Count(*) as cnt FROM [SurveyData] WHERE  [Status]='S' AND [EventId]=? AND [SectionId]=? ";
        if (subSec.trim().length() > 0)
            sql += (" AND [SubSectionId]='" + subSec + "'");
        if (vono.trim().length() > 0)
            sql += (" AND [OrgNo]='" + vono + "'");
        if (memNo.trim().length() > 0)
            sql += (" AND [OrgMemNo]='" + memNo + "'");

        int cnt = getRecordCount(sql, new String[]{eventId.toString(), sec.toString()});
        return (cnt > 0);
    }

    public boolean hasServeyData(Integer eventId, Integer sec, String subSec, String vono, String memNo) {
        String sql = "SELECT Count(*) as cnt FROM [SurveyData] WHERE  [EventId]=? AND [SectionId]=? ";
        if (subSec.trim().length() > 0)
            sql += (" AND [SubSectionId]='" + subSec + "'");
        if (vono.trim().length() > 0)
            sql += (" AND [OrgNo]='" + vono + "'");
        if (memNo.trim().length() > 0)
            sql += (" AND [OrgMemNo]='" + memNo + "'");

        int cnt = getRecordCount(sql, new String[]{eventId.toString(), sec.toString()});
        return (cnt > 0);
    }

    public void updateRespondent(Respondent r) {
        ContentValues cv = new ContentValues();

        try {
            Respondent rsp = getRespondent(r.getEventId(), r.getSectionId(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo());
            if (r.getStatus().equals("N") || r.getStatus().equals("U") || r.getStatus().equals("S")) {
                cv.put("[EventId]", r.getEventId());
                cv.put("[SectionId]", r.getSectionId());
                cv.put("[SubSectionId]", r.getSubSectionId());
                cv.put("[OrgNo]", r.getOrgNo());
                cv.put("[OrgMemNo]", r.getOrgMemNo());
                cv.put("[MemberName]", r.getMemberName());
                cv.put("[AdmissionDate]", P8.getDateTime(r.getAdmissionDate()));
                cv.put("[LoanNo]", r.getLoanNo());
                cv.put("[DisbDate]", P8.getDateTime(r.getDisbDate()));
                cv.put("[Amount]", r.getAmount());
                cv.put("[CONo]", r.getCONo());
                cv.put("[COName]", r.getCOName());
                cv.put("[Status]", r.getStatus());
                cv.put("[MonitorNo]", r.getMonitorNo());

                if (rsp == null) {
                    // Add
                    db.insert(DBHelper.TBL_RESPONDENTS, null, cv);
                } else {
                    cv.put("[Status]", r.getStatus());
                    cv.put(" [Updated_At]", P8.getDateTime());
                    db.update(DBHelper.TBL_RESPONDENTS, cv, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=?",
                            new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId().toString(), r.getOrgNo(), r.getOrgMemNo()});
                }
            } else if (r.getStatus().equals("D")) {
                if (rsp.getStatus().equals("N")) {
                    db.delete(DBHelper.TBL_RESPONDENTS, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=?",
                            new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId().toString(), r.getOrgNo(), r.getOrgMemNo()});
                } else {
                    cv.put("[Status]", r.getStatus());
                    cv.put(" [Updated_At]", P8.getDateTime());
                    db.update(DBHelper.TBL_RESPONDENTS, cv, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=?",
                            new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId().toString(), r.getOrgNo(), r.getOrgMemNo()});
                }
            }
        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void updateRespondentStatus(Respondent r) {
        ContentValues cv = new ContentValues();

        try {
            if (r.getStatus().equals("S")) {
                Respondent rsp = getRespondent(r.getEventId(), r.getSectionId(), r.getOrgNo(), r.getOrgMemNo());
                if (rsp != null)
                    if (rsp.getStatus().equals("N") || rsp.getStatus().equals("U")) {
                        cv.put("[Id]", r.getId());
                        cv.put("[Status]", r.getStatus());
                        cv.put(" [Updated_At]", P8.getDateTime());
                        db.update(DBHelper.TBL_RESPONDENTS, cv, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=?",
                                new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId().toString(), r.getOrgNo(), r.getOrgMemNo()});
                    } else if (rsp.getStatus().equals("D")) {
                        db.delete(DBHelper.TBL_RESPONDENTS, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=?",
                                new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId().toString(), r.getOrgNo(), r.getOrgMemNo()});
                    }
            }
        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

    }


    public ArrayList<Respondent> getMemberListCurrentAll() {
        ArrayList<Respondent> ret = new ArrayList<Respondent>();
        Cursor curs = null;
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[lnStatus]=0 GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";

        try {
            curs = db.rawQuery(sql, null);
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                do {
                    Respondent rsp = new Respondent();
                    rsp.setOrgNo(curs.getString(0));
                    rsp.setOrgMemNo(curs.getString(1));
                    rsp.setMemberName(curs.getString(2));
                    rsp.setAdmissionDate(curs.getString(3));
                    rsp.setLoanNo(curs.getInt(4));
                    rsp.setDisbDate(curs.getString(5));
                    rsp.setAmount(curs.getInt(6));
                    rsp.setCONo(curs.getString(7));
                    rsp.setCOName(curs.getString(8));
                    rsp.setStatus("N");
                    ret.add(rsp);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;

    }

    public ArrayList<Respondent> getMemberList(String sql) {
        ArrayList<Respondent> ret = new ArrayList<Respondent>();
        Cursor curs = null;

        try {
            curs = db.rawQuery(sql, null);
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                do {
                    Respondent rsp = new Respondent();
                    rsp.setOrgNo(curs.getString(0));
                    rsp.setOrgMemNo(curs.getString(1));
                    rsp.setMemberName(curs.getString(2));
                    rsp.setAdmissionDate(curs.getString(3));
                    rsp.setLoanNo(curs.getInt(4));
                    rsp.setDisbDate(curs.getString(5));
                    rsp.setAmount(curs.getInt(6));
                    rsp.setCONo(curs.getString(7));
                    rsp.setCOName(curs.getString(8));
                    rsp.setStatus("N");
                    ret.add(rsp);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public ArrayList<Respondent> getMemberListAll() {
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";
        return getMemberList(sql);
    }

    public ArrayList<Respondent> getMemberListSection_4_11() {
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[Overdue] < 0 GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";
        return getMemberList(sql);
    }

    public ArrayList<Respondent> getMemberListSection_4_10() {
        String toDate = P8.FormatDate(ActiveUser.getInstance().getDateStart(), "yyyy-MM-dd");
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[lnStatus]=0 AND (julianday('" + toDate + "')-julianday(L.[DisbDate])) <= 30 GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";
        return getMemberList(sql);
    }

    public ArrayList<Respondent> getMemberListSection_4_9() {
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN ClosedLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";

        return getMemberList(sql);
    }


    public ArrayList<Respondent> getMemberListLateNIBL1All() {
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[lnStatus]=3 OR L.[lnStatus]=4 GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";
        return getMemberList(sql);
    }

    public ArrayList<Respondent> getMemberListNIBL2All() {
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[lnStatus]=5 GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";

        return getMemberList(sql);
    }


    public ArrayList<Respondent> getMemberListSection_2(String vono, Integer monNo) {
        ArrayList<Respondent> ret = new ArrayList<Respondent>();
        Cursor curs = null;
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[lnStatus]=0 AND M.[OrgNo]=? " +
                "GROUP BY M.[OrgNo], M.[OrgMemNo] " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";

        try {
            curs = db.rawQuery(sql, new String[]{vono});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                do {
                    Integer n = Integer.parseInt(curs.getString(1)) % 2;
                    if ((monNo == 1 && n == 1) || monNo == 2 && n == 0) {
                        Respondent rsp = new Respondent();
                        rsp.setOrgNo(curs.getString(0));
                        rsp.setOrgMemNo(curs.getString(1));
                        rsp.setMemberName(curs.getString(2));
                        rsp.setAdmissionDate(curs.getString(3));
                        rsp.setLoanNo(curs.getInt(4));
                        rsp.setDisbDate(curs.getString(5));
                        rsp.setAmount(curs.getInt(6));
                        rsp.setCONo(curs.getString(7));
                        rsp.setCOName(curs.getString(8));
                        rsp.setStatus("N");
                        ret.add(rsp);
                    }
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public ArrayList<Respondent> getMemberListSection_1() {
        ArrayList<Respondent> ret = new ArrayList<Respondent>();
        Cursor curs = null;
        String sql = "SELECT M.[OrgNo], M.[OrgMemNo], M.[MemberName], M.[AdmissionDate], L.[LoanNo], L.[DisbDate], L.[PrincipalAmt], V.[CONo], C.[COName] " +
                "FROM ((CMembers M INNER JOIN CLoans L ON M.[OrgNo]=L.[OrgNo] AND M.[OrgMemNo]=L.[OrgMemNo]) " +
                "INNER JOIN VOList V ON M.[OrgNo]= V.[OrgNo]) " +
                "INNER JOIN COList C ON trim(V.[CONo]) =  trim(C.[CONo]) " +
                "WHERE L.[LoanSlNo]=1 AND DATE(M.[AdmissionDate])>=? " +
                "ORDER BY M.[OrgNo], M.[OrgMemNo]";

        ActiveUser usr = ActiveUser.getInstance();
        //if(!usr.isActiveEvent()) return ret;

        Calendar c = Calendar.getInstance();
        c.setTime(usr.getDataEndDate());
        c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);                //First day of last week
        c.add(Calendar.DAY_OF_YEAR, -14);                            //Total 3 weeks

        //
        int i = 3;
        try {
            while (i < 48) {
                curs = db.rawQuery(sql, new String[]{P8.FormatDate(c.getTime(), "yyyy-MM-dd")});
                curs.moveToFirst();
                if (curs.getCount() >= 10)
                    break;
                else
                    c.add(Calendar.DAY_OF_YEAR, -7);
                i++;
            }
            if (!curs.isAfterLast()) {
                do {
                    Respondent rsp = new Respondent();
                    rsp.setOrgNo(curs.getString(0));
                    rsp.setOrgMemNo(curs.getString(1));
                    rsp.setMemberName(curs.getString(2));
                    rsp.setAdmissionDate(curs.getString(3));
                    rsp.setLoanNo(curs.getInt(4));
                    rsp.setDisbDate(curs.getString(5));
                    rsp.setAmount(curs.getInt(6));
                    rsp.setCONo(curs.getString(7));
                    rsp.setCOName(curs.getString(8));
                    rsp.setStatus("N");
                    ret.add(rsp);

                } while (curs.moveToNext());
            }


        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return ret;
    }

    public SurveyData getSurveyData(Integer eventId, Integer sec, String subsec, String vono, String memNo, Integer qno) {
        return getSurveyData(eventId, sec, subsec, vono, memNo, qno, -1);
    }

    public SurveyData getSurveyData(Integer eventId, Integer sec, String subsec, String vono, String memNo, Integer qno, Integer monNo) {
        SurveyData sd = null;
        Cursor curs = null;

        String sql = "SELECT [Id], [EventId], [SectionId], [OrgNo], [OrgMemNo], [Question], [Answer], [Score], [Status], [Updated_At], [SubSectionId] " +
                "FROM SurveyData " +
                "WHERE [EventId]=? AND [SectionId]=?  AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=? AND [Question] = ?";
        if (monNo > -1)
            sql += (" AND [MonitorNo]=" + monNo);

        try {
            curs = db.rawQuery(sql, new String[]{eventId.toString(), sec.toString(), subsec, vono, memNo, qno.toString()});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                sd = new SurveyData();
                sd.setId(curs.getInt(0));
                sd.setEventId(curs.getInt(1));
                sd.setSectionId(curs.getInt(2));
                sd.setOrgNo(curs.getString(3));
                sd.setOrgMemNo(curs.getString(4));
                sd.setQuestion(curs.getInt(5));
                sd.setAnswer(curs.getInt(6));
                sd.setScore(curs.getInt(7));
                sd.setStatus(curs.getString(8));
                sd.setUpdated_At(curs.getString(9));
                sd.setSubSectionId(curs.getString(10));
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return sd;
    }

    public ArrayList<SurveyData> getSurveyData(Integer eventId, boolean onlyChanged) {
        return getSurveyData(eventId, 0, onlyChanged);
    }

    public ArrayList<SurveyData> getSurveyData(Integer eventId, Integer sec, boolean onlyChanged) {
        return getSurveyData(eventId, sec, "", onlyChanged);
    }

    public ArrayList<SurveyData> getSurveyData(Integer eventId, Integer sec, String subsec, boolean onlyChanged) {
        return getSurveyData(eventId, sec, subsec, "", "", onlyChanged);
    }

    public ArrayList<SurveyData> getSurveyData(Integer eventId, Integer sec, String subsec, String vono, String memNo) {
        return getSurveyData(eventId, sec, subsec, vono, memNo, false);
    }

    public ArrayList<SurveyData> getSurveyData(Integer eventId, Integer sec, String subsec, String vono, String memNo, boolean onlyChanged) {
        return getSurveyData(eventId, sec, subsec, vono, memNo, -1, onlyChanged);
    }

    public ArrayList<SurveyData> getSurveyData(Integer eventId, Integer sec, String subsec, String vono, String memNo, Integer monNo, boolean onlyChanged) {
        ArrayList<SurveyData> ret = new ArrayList<SurveyData>();
        Cursor curs = null;

        String sql = "SELECT [Id], [EventId], [SectionId], [OrgNo], [OrgMemNo], [Question], [Answer], [Score], [Status], [Updated_At], [SubSectionId], [Remarks], [MonitorNo] " +
                "FROM SurveyData " +
                "WHERE [EventId]=? "; //AND [SectionId]=? AND  [SubSectionId] = ? AND [OrgNo]=? AND [OrgMemNo]=? " ;
        if (sec > 0)
            sql += (" AND [SectionId]=" + sec);
        if (subsec.trim().length() > 0)
            sql += (" AND [SubSectionId]='" + subsec + "'");
        if (vono.trim().length() > 0)
            sql += (" AND [OrgNo]='" + vono + "'");
        if (memNo.trim().length() > 0)
            sql += (" AND [OrgMemNo]='" + memNo + "'");
        if (monNo > -1)
            sql += (" AND [MonitorNo]=" + monNo);


        if (onlyChanged) {
            sql += " AND [Status] <> 'S'";
        } else {
            sql += " AND [Status] <> 'D'";
        }

        sql += " ORDER BY [Question]";

        try {
            curs = db.rawQuery(sql, new String[]{eventId.toString()});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                do {
                    SurveyData sd = new SurveyData();
                    sd.setId(curs.getInt(0));
                    sd.setEventId(curs.getInt(1));
                    sd.setSectionId(curs.getInt(2));
                    sd.setOrgNo(curs.getString(3));
                    sd.setOrgMemNo(curs.getString(4));
                    sd.setQuestion(curs.getInt(5));
                    sd.setAnswer(curs.getInt(6));
                    sd.setScore(curs.getInt(7));
                    sd.setStatus(curs.getString(8));
                    sd.setUpdated_At(curs.getString(9));
                    sd.setSubSectionId(curs.getString(10));
                    sd.setRemarks(curs.getString(11));
                    sd.setMonitorNo(curs.getInt(12));

                    Log.d(TAG, sd.toString());
                    ret.add(sd);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public void updateSurveyDataStatus(SurveyData r) {
        ContentValues cv = new ContentValues();

        try {
            if (r.getStatus().equals("S")) {
                SurveyData rsp = getSurveyData(r.getEventId(), r.getSectionId(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion(), r.getMonitorNo());
                if (rsp != null)
                    if (rsp.getStatus().equals("N") || rsp.getStatus().equals("U")) {
                        cv.put("[Id]", r.getId());
                        cv.put("[Status]", r.getStatus());
                        cv.put(" [Updated_At]", P8.getDateTime());
                        db.update(DBHelper.TBL_SURVEYDATA, cv, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=? AND [Question]=? AND [MonitorNo]=?",
                                new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion().toString(), r.getMonitorNo().toString()});
                    } else if (rsp.getStatus().equals("D")) {
                        db.delete(DBHelper.TBL_SURVEYDATA, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=? AND [Question]=? AND [MonitorNo]=?",
                                new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion().toString(), r.getMonitorNo().toString()});
                    }
            }
        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void clearOtherEventsData(Integer eventId) {
        try {
            db.delete(DBHelper.TBL_RESPONDENTS, "eventId <> ?", new String[]{eventId.toString()});
            db.delete(DBHelper.TBL_SURVEYDATA, "eventId <> ?", new String[]{eventId.toString()});
        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public void updateSurveyData(SurveyData r) {
        ContentValues cv = new ContentValues();

        try {
            SurveyData sd = getSurveyData(r.getEventId(), r.getSectionId(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion(), r.getMonitorNo());
            if (r.getStatus().equals("N") || r.getStatus().equals("U") || r.getStatus().equals("S")) {
                cv.put("[Id]", r.getId());
                cv.put("[EventId]", r.getEventId());
                cv.put("[SectionId]", r.getSectionId());
                cv.put("[OrgNo]", r.getOrgNo());
                cv.put("[OrgMemNo]", r.getOrgMemNo());
                cv.put("[Question]", r.getQuestion());
                cv.put("[Answer]", r.getAnswer());
                cv.put("[Score]", r.getScore());
                cv.put("[Remarks]", r.getRemarks());
                cv.put("[MonitorNo]", r.getMonitorNo());
                cv.put("[Status]", r.getStatus());
                cv.put("[SubSectionId]", r.getSubSectionId());

                if (sd == null) {
                    // Add
                    db.insert(DBHelper.TBL_SURVEYDATA, null, cv);
                } else {
                    if (sd.getStatus().equals("S"))
                        r.setStatus("U");
                    else
                        r.setStatus(sd.getStatus());

                    cv.put("[Status]", r.getStatus());
                    cv.put(" [Updated_At]", P8.getDateTime());
                    db.update(DBHelper.TBL_SURVEYDATA, cv, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=? AND [Question]=? AND [MonitorNo]=?",
                            new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion().toString(), r.getMonitorNo().toString()});
                }
            } else if (r.getStatus().equals("D")) {
                if (sd.getStatus().equals("N")) {
                    db.delete(DBHelper.TBL_SURVEYDATA, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=? AND [Question]=? AND [MonitorNo]=?",
                            new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion().toString(), r.getMonitorNo().toString()});
                } else {
                    cv.put("[Status]", r.getStatus());
                    cv.put(" [Updated_At]", P8.getDateTime());
                    db.update(DBHelper.TBL_RESPONDENTS, cv, "[EventId]=? AND [SectionId]=? AND [SubSectionId]=? AND [OrgNo]=? AND [OrgMemNo]=? AND [Question]=? AND [MonitorNo]=?",
                            new String[]{r.getEventId().toString(), r.getSectionId().toString(), r.getSubSectionId(), r.getOrgNo(), r.getOrgMemNo(), r.getQuestion().toString(), r.getMonitorNo().toString()});
                }
            }
        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    // Get target VO List for Section 2
    public ArrayList<HashMap<String, String>> getVOListForSection2(Integer monNo, Date dtStart, Date dtEnd) {

        ArrayList<HashMap<String, String>> itms = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
//		"HAVING Count(*) > 20 " +
        String sql = "SELECT V.[OrgNo], V.[OrgName], V.[TargetDate], L.MemCnt, L.MissedCnt, V.[CONo], C.[COName] " +    //, C.[COName]
                "FROM (VOList V INNER JOIN ( " +
                "SELECT [OrgNo], Count(*) as MemCnt, Sum(CASE WHEN [Overdue] > 0 THEN 1 ELSE 0 END) as  MissedCnt FROM CLoans WHERE [LnStatus] = 0 GROUP BY [OrgNo] " +
                ") L ON V.[OrgNo] = L.[OrgNo]) INNER JOIN COLIST C ON Trim(V.[CONo])= Trim(C.[CONo] ) " +                                        //  INNER JOIN COLIST C ON V.[CONo]=C.[CONo]
                "WHERE Date(V.[TargetDate]) BETWEEN ? AND ? " +
                "ORDER BY MissedCnt DESC LIMIT 6";

        try {

            curs = db.rawQuery(sql, new String[]{P8.FormatDate(dtStart, "yyyy-MM-dd"), P8.FormatDate(dtEnd, "yyyy-MM-dd")});
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                    map.put(DBHelper.FLD_ORG_NAME, curs.getString(1));
                    map.put(DBHelper.FLD_TARGETDATE, P8.FormatDate(P8.ConvertStringToDate(curs.getString(2), "yyyy-MM-dd"), "dd-MMM-yyyy"));
                    map.put("TotalMember", "" + curs.getInt(3));
                    map.put("MissedCount", "" + curs.getInt(4));
                    map.put("isSelected", "0");
                    map.put("hasMember", "0");
                    map.put("othersSeleced", "0");
                    map.put(DBHelper.FLD_CONO, curs.getString(5));
                    map.put(DBHelper.FLD_CONAME, curs.getString(6));

                    itms.add(map);
                } while (curs.moveToNext());
            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        } finally {
            if (curs != null)
                curs.close();
        }
        return itms;
    }


    public PO getPO() {
        PO cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_PO, new String[]{"CONo", "COName",
                    "SessionNo", "OpenDate", "OpeningBal", "Password",
                    "EMethod", "CashInHand", "EnteredBy", "SYSDATE", "SYSTIME",
                    "AltCOName", "BranchCode", "BranchName", "BODate",
                    "BOStatus"}, null, null, null, null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cl = new PO(curs.getString(0), curs.getString(1),
                        curs.getInt(2), curs.getString(3), curs.getInt(4),
                        curs.getString(5), curs.getInt(6), curs.getInt(7),
                        curs.getString(8), curs.getString(9),
                        curs.getString(10), curs.getString(11),
                        curs.getString(12), curs.getString(13),
                        curs.getString(14), curs.getInt(15));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return cl;

    }

    public CO getCO(String cono) {
        CO co = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_COLIST, new String[]{"[CONo]", "[COName]"}, "[CONo]=?", new String[]{cono.trim()},
                    null, null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                co = new CO(curs.getString(0), curs.getString(1));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return co;
    }

    public List<CO> getCOList() {
        List<CO> list = new ArrayList<CO>();
        CO co = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_COLIST, new String[]{"[CONo]", "[COName]", "[LastPOSyncTime]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    co = new CO(curs.getString(0), curs.getString(1), P8.ConvertStringToDate(curs.getString(2)));
                    list.add(co);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return list;
    }

    public Integer getCOCount(String cono) {
        Integer cnt = 0;
        Cursor curs = null;
        if (cono == null || cono.isEmpty())
            cono = "%";
        try {
            curs = db.rawQuery("SELECT COUNT(*) AS cnt FROM "
                            + DBHelper.TBL_COLIST + " WHERE [CONO] LIKE ?",
                    new String[]{cono});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cnt = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return cnt;
    }

    public List<VO> getVOList() {
        return getTodaysVO(null);
    }

    public List<VO> getVOList(String cono) {
        List<VO> list = new ArrayList<VO>();
        VO vo = null;
        Cursor curs = null;
        String Whereclause = null;
        String[] WhereValue = null;
        if (cono != null && !cono.isEmpty()) {
            Whereclause = "[CONo] == ?";
            WhereValue = new String[]{P8.padLeft(cono, 8)};
        }
        try {
            curs = db.query(DBHelper.TBL_VOLIST, new String[]{"[OrgNo]",
                    "[OrgName]", "[OrgCategory]", "[MemSexID]",
                    "[SavColcOption]", "[LoanColcOption]", "[SavColcDate]",
                    "[LoanColcDate]", "[CONo]"}, Whereclause, WhereValue, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    vo = new VO(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getInt(3), curs.getInt(4),
                            curs.getInt(5), P8.ConvertStringToDate(
                            curs.getString(6), "yyyy-MM-dd"),
                            P8.ConvertStringToDate(curs.getString(7),
                                    "yyyy-MM-dd"), curs.getString(8));

                    list.add(vo);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return list;
    }

    public List<VO> getTodaysVO(List<VO> srcVO) {
        List<VO> lst = new ArrayList<VO>();
        if (srcVO == null || srcVO.isEmpty())
            srcVO = getVOList();

        for (VO vo : srcVO) {
            if (vo.isTodaysVO()) {
                lst.add(vo);
            }
        }
        return lst;
    }

    public VO getVO(String vono) {
        VO vo = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_VOLIST, new String[]{"[OrgNo]",
                            "[OrgName]", "[OrgCategory]", "[MemSexID]",
                            "[SavColcOption]", "[LoanColcOption]", "[SavColcDate]",
                            "[LoanColcDate]", "[CONo]"}, "[OrgNo]=?", new String[]{vono},
                    null, null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                vo = new VO(curs.getString(0), curs.getString(1),
                        curs.getString(2), curs.getInt(3), curs.getInt(4),
                        curs.getInt(5), P8.ConvertStringToDate(
                        curs.getString(6), "yyyy-MM-dd"),
                        P8.ConvertStringToDate(curs.getString(7), "yyyy-MM-dd"),
                        curs.getString(8));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return vo;
    }

    public Integer getRecordCount(String tblSql, String[] cond) {
        Integer cnt = 0;
        Cursor curs = null;

        //Log.d(TAG, "getRecordCount: " + tblSql);

        try {
            curs = db.rawQuery(tblSql, cond);
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cnt = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cnt;
    }

    public Integer getMemberCount(String VONo) {
        return getMemberCount(VONo, null);
    }

    public Integer getMemberCount(String VONo, String CONo) {
        Integer cnt = 0;

        if ((VONo == null || VONo.isEmpty()) &&
                (CONo == null || CONo.isEmpty()))
            cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                    + DBHelper.TBL_CMEMBERS, null);
        else
            cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                            + DBHelper.TBL_CMEMBERS + " M INNER JOIN  " + DBHelper.TBL_VOLIST +
                            " V ON M.OrgNo=V.OrgNo WHERE M.[OrgNo] LIKE ? AND V.CONo LIKE ?",
                    new String[]{P8.getVOCondition(VONo), P8.getPOCondition(CONo)});

        return cnt;
    }

	/*
     * public Integer getTodaysVOCount() { return
	 * getRecordCount("SELECT Count(*) as cnt FROM " + DBHelper.TBL_VOLIST +
	 * " WHERE ", cond) }
	 */

//	public Integer getVOCount(String vono) {
//		return  getVOCount(vono, null);
//	}

    public Integer getVOCount(String vono, String cono) {
        Integer cnt = 0;
        Cursor curs = null;
//		if (vono == null || vono.isEmpty())
//			vono = "%";
        vono = P8.getVOCondition(vono);
        cono = P8.getPOCondition(cono);

        try {
            curs = db.rawQuery("SELECT COUNT(*) AS cnt FROM "
                            + DBHelper.TBL_VOLIST + " WHERE [OrgNO] LIKE ? AND [CONo] LIKE ?",
                    new String[]{vono, cono});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cnt = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cnt;

    }

    // Returns: returns array of integer
    //	Arguments:
    //		vono - VO number, or null for all
    //		cono - CO number or null for all
    //
    //	Returns:
    // 	subscript 	0 - Total borrowers
    //				1 -  Total current borrower
    //				2 - Total loans
    public Integer[] getTotalBorrowsers(String vono, String cono) {
        Cursor curs = null;
        Integer[] ret = {0, 0, 0};

        String[] vv = {P8.getVOCondition(vono), P8.getPOCondition(cono)};

        try {
            curs = db
                    .rawQuery(
                            "select count(*) AS BORROWER, sum(LNCount) AS MCNT,  sum(CURLNS) AS CURBOR "
                                    + "from (SELECT   L.[OrgNo], L.[OrgMemNo], Count(L.[LoanNo]) as LNCount,  "
                                    + "Max(CASE WHEN IfNull(L.[LoanNo], 0) > 0 AND IfNull(L.[LnStatus],0) = 0 THEN 1 ELSE 0 END) as CURLNS "
                                    + "FROM CLoans L INNER JOIN VOList V ON L.[OrgNo]=V.[OrgNo] "
                                    + "WHERE L.[OrgNo] LIKE ? AND V.[CONo] LIKE ? AND L.[LB] > 0 group by L.OrgNo, L.OrgMemNo )",
                            vv);
            if (curs.moveToFirst()) {
                ret[0] = curs.getInt(0);
                ret[1] = curs.getInt(2);
                ret[2] = curs.getInt(1);
            }
            curs.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return ret;
    }

    public ArrayList<HashMap<String, String>> getMemberList(DBoardItem dbitm, String vono, String cono) {
        String sql = "";
        String[] param = null;

//		Log.d(TAG, "DBITEM: " + dbitm.toString() + "  VONO: "
//				+ ((vono == null) ? "null" : vono)
//				+ " CO/PO: " + ((cono == null) ? "null" : cono));

        switch (dbitm) {
            case MEMBERS:
                sql = "SELECT M." + DBHelper.FLD_ORG_NO + ", M."
                        + DBHelper.FLD_ORG_MEM_NO + ", M."
                        + DBHelper.FLD_ORG_MEM_NAME + " " + "FROM "
                        + DBHelper.TBL_CMEMBERS + " M INNER JOIN "
                        + DBHelper.TBL_VOLIST + " V ON M."
                        + DBHelper.FLD_ORG_NO + "=V." + DBHelper.FLD_ORG_NO;
                if (!(vono == null || vono.length() == 0)) {
                    sql += " WHERE M." + DBHelper.FLD_ORG_NO + " = ?";
                    param = new String[]{vono};
                } else if (!(cono == null || cono.length() == 0)) {
                    sql += " WHERE V." + DBHelper.FLD_CONO + " = ?";
                    param = new String[]{P8.getPOCondition(cono)};
                }
//			Log.d(TAG, sql);
                break;

            case BORROWERS:
            case LOANS:
            case OUTLOANS:
                sql = MembersSql(vono, cono);
//			Log.d(TAG, sql);
                break;

            case CURBORROWERS:
            case CURLOANS:
                sql = MembersSql(vono, cono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_CURRENT;

                break;
            case L1LOANS:
                sql = MembersSql(vono, cono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_LATE1;
                break;
            case L2LOANS:
                sql = MembersSql(vono, cono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_LATE2;
                break;
            case N1LOANS:
                sql = MembersSql(vono, cono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_NIBL1;
                break;
            case N2LOANS:
                sql = MembersSql(vono, cono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_NIBL2;
                break;
            case OVERLOANS:
                sql = MembersSql(vono, cono) + " AND LN." + DBHelper.FLD_OVERDUE_BAL
                        + " > 0 AND LN." + DBHelper.FLD_LOAN_STATUS + "="
                        + LS_CURRENT;
                break;
            case PO_CASH_IN_HAND:
                break;
            case REPAY_YET_COLLECTED:
                sql = MembersSql(vono, " AND IfNull(LN.IAB, 0) > 0", cono);
                break;
            case REPAY_TARGET:
                sql = MembersSql(vono, cono) + " AND IfNull(LN.TargetAmtLoan, 0) > 0";
                break;
            case SAVE_YET_COLLECTED:
                sql = MembersSql(vono, cono) + " AND IfNull(CM.SIA, 0) > 0";
                break;
            case SAVE_TARGET:
                sql = MembersSql(vono, cono) + " AND IfNull(CM.TargetAmtSav, 0) > 0";
                break;
            case VOS:
                break;
            default:
                break;

        }
        return getMemberList(sql + ";", param);
    }

//	private String MembersSql(String vono) {
//		return MembersSql(vono, null);
//	}

    private String MembersSql(String vono, String cono) {

        String sql = "SELECT DISTINCT CM." + DBHelper.FLD_ORG_NO + ", CM."
                + DBHelper.FLD_ORG_MEM_NO + ", CM." + DBHelper.FLD_ORG_MEM_NAME
                + " FROM " + DBHelper.TBL_CMEMBERS + " CM INNER JOIN "
                + DBHelper.TBL_CLOANS + " LN ON CM." + DBHelper.FLD_ORG_NO
                + " = LN." + DBHelper.FLD_ORG_NO + " AND CM."
                + DBHelper.FLD_ORG_MEM_NO + " = LN." + DBHelper.FLD_ORG_MEM_NO
                + " INNER JOIN " + DBHelper.TBL_VOLIST + " V ON CM." + DBHelper.FLD_ORG_NO
                + "=V." + DBHelper.FLD_ORG_NO;
        sql += " WHERE IfNull(LN." + DBHelper.FLD_LOAN_NO + ", 0) > 0";
        if (!(vono == null || vono.length() == 0)) {
            sql += " AND CM." + DBHelper.FLD_ORG_NO + " = '" + vono + "'";
        } else if (!(cono == null || cono.length() == 0)) {
            sql += " AND V." + DBHelper.FLD_CONO + " = '" + P8.getPOCondition(cono) + "'";
        }

        return sql;
    }

    // Member list segregated by loan status, used only for loans, yet to loan
    // collections
    private String MembersSql(String vono, String ExtraCond, String cono) {

        String sql = "SELECT CM." + DBHelper.FLD_ORG_NO + ", CM."
                + DBHelper.FLD_ORG_MEM_NO + ", CM." + DBHelper.FLD_ORG_MEM_NAME
                + ", LN." + DBHelper.FLD_LOAN_NO + ", LN."
                + DBHelper.FLD_LOAN_STATUS + ", ST."
                + DBHelper.FLD_LOAN_STATUS_NAME + " FROM "
                + DBHelper.TBL_CMEMBERS + " CM INNER JOIN "
                + DBHelper.TBL_CLOANS + " LN ON CM." + DBHelper.FLD_ORG_NO
                + " = LN." + DBHelper.FLD_ORG_NO + " AND CM."
                + DBHelper.FLD_ORG_MEM_NO + " = LN." + DBHelper.FLD_ORG_MEM_NO
                + " INNER JOIN " + DBHelper.TBL_LOANSTATUS + " ST ON LN."
                + DBHelper.FLD_LOAN_STATUS + " = ST." + DBHelper.FLD_LOAN_STATUS
                + " INNER JOIN " + DBHelper.TBL_VOLIST + " V ON CM." + DBHelper.FLD_ORG_NO
                + "=V." + DBHelper.FLD_ORG_NO;
        sql += " WHERE IfNull(LN." + DBHelper.FLD_LOAN_NO + ", 0) > 0";
        if (!(vono == null || vono.length() == 0)) {
            sql += " AND CM." + DBHelper.FLD_ORG_NO + " = '" + vono + "'";
        } else if (!(cono == null || cono.length() == 0)) {
            sql += " AND V." + DBHelper.FLD_CONO + " = '" + P8.getPOCondition(cono) + "'";
        }

        if (!ExtraCond.isEmpty())
            sql += ExtraCond;
        // add order by
        sql += " ORDER BY LN." + DBHelper.FLD_LOAN_STATUS;
        return sql;

    }

    public ArrayList<HashMap<String, String>> getMemberList(String strSql, String[] args) {
        ArrayList<HashMap<String, String>> itms = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
//		Log.d(TAG, "From second getMemberList - " + strSql);
        try {
            curs = db.rawQuery(strSql, args);

            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                if (curs.getColumnCount() <= 3) {
                    do {
                        map = new HashMap<String, String>();
                        map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                        map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                        map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));
                        map.put(DBHelper.FLD_LOAN_NO, "");
                        map.put(DBHelper.FLD_LOAN_STATUS_NAME, "");
                        itms.add(map);
                    } while (curs.moveToNext());
                } else {
                    int st = -100;
                    do {
                        if (st != curs.getInt(4)) {
                            // Header row
                            st = curs.getInt(4);
                            map = new HashMap<String, String>();
                            map.put(DBHelper.FLD_ORG_NO, "");
                            map.put(DBHelper.FLD_ORG_MEM_NO, "");
                            map.put(DBHelper.FLD_ORG_MEM_NAME, "");
                            map.put(DBHelper.FLD_LOAN_NO, "");
                            map.put(DBHelper.FLD_LOAN_STATUS_NAME,
                                    curs.getString(5));
                            itms.add(map);
                        }
                        map = new HashMap<String, String>();
                        map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                        map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                        map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));
                        map.put(DBHelper.FLD_LOAN_NO, curs.getString(3));
                        map.put(DBHelper.FLD_LOAN_STATUS_NAME, "");
                        itms.add(map);
                    } while (curs.moveToNext());
                }

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return itms;
    }

    public Cursor getMemberList(final CharSequence constraint,
                                final String strSql, final String fixedWheres) {
        String whr = "";
        String sql = "";
        if (strSql == null || strSql.length() == 0) {
            sql = "SELECT " + DBHelper.TBL_CMEMBERS + "." + DBHelper.FLD_ORG_NO
                    + ", " + DBHelper.TBL_CMEMBERS + "."
                    + DBHelper.FLD_ORG_MEM_NO + ", " + DBHelper.TBL_CMEMBERS
                    + "." + DBHelper.FLD_ORG_MEM_NAME;
            ;
        } else
            sql = strSql;

        if (!(fixedWheres == null || fixedWheres.length() == 0))
            whr = "(" + fixedWheres + ")";

        String[] selargs = null;
        if (!(constraint == null || constraint.length() == 0)) {
            String value = "%" + constraint + "%";
            if (whr.length() > 0)
                whr += " AND (" + DBHelper.TBL_CMEMBERS + "."
                        + DBHelper.FLD_ORG_MEM_NO + " LIKE ? OR "
                        + DBHelper.TBL_CMEMBERS + "."
                        + DBHelper.FLD_ORG_MEM_NAME + " LIKE ?)";
            selargs = new String[]{value, value};
        }

        if (whr.length() > 0)
            whr = " WHERE " + whr;
        sql = sql + whr;
        return db.rawQuery(sql, selargs);
    }

    public ArrayList<CMember> getAllCMemebers() {
        CMember cm = null;
        Cursor curs = null;

        ArrayList<CMember> cMemList = new ArrayList<CMember>();

        try {
            curs = db.query(DBHelper.TBL_CMEMBERS, new String[]{
                            "[ProjectCode]", "[OrgNo]", "[OrgMemNo]", "[MemberName]",
                            "[SavBalan]", "[SavPayable]", "[CalcIntrAmt]",
                            "[TargetAmtSav]", "[ColcDate]", "[ReceAmt]", "[AdjAmt]",
                            "[SB]", "[SIA]", "[SavingsRealized]", "[memSexID]", "[NationalIDNo]",
                            "[PhoneNo]", "[WalletNo]", "[AdmissionDate]"}, null, null, null,
                    null, null);

            if (curs.moveToFirst()) {
                do {
                    cm = new CMember(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getString(3),
                            curs.getInt(4), curs.getInt(5), curs.getDouble(6),
                            curs.getInt(7), P8.ConvertStringToDate(
                            curs.getString(8), "yyyy-MM-dd"),
                            curs.getInt(9), curs.getInt(10), curs.getInt(11),
                            curs.getInt(12), curs.getInt(13), curs.getInt(14),
                            curs.getString(15), curs.getString(16), curs.getString(17),
                            P8.ConvertStringToDate(curs.getString(18), "yyyy-MM-dd"));

                    cMemList.add(cm);
                } while (curs.moveToNext());

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cMemList;
    }

    public CMember getCMember(String vono, String memno) {
        CMember cm = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_CMEMBERS, new String[]{
                            "[ProjectCode]", "[OrgNo]", "[OrgMemNo]", "[MemberName]",
                            "[SavBalan]", "[SavPayable]", "[CalcIntrAmt]",
                            "[TargetAmtSav]", "[ColcDate]", "[ReceAmt]", "[AdjAmt]",
                            "[SB]", "[SIA]", "[SavingsRealized]", "[memSexID]", "IfNull([NationalIDNo],'')",
                            "IfNull([PhoneNo],'')", "IfNull([WalletNo],'')", "IfNull([AdmissionDate], '2001-01-01 12:00:00')"},
                    "[OrgNo]=? AND [OrgMemNo]=?", new String[]{vono, memno},
                    null, null, null);
            if (curs.moveToFirst()) {
                cm = new CMember(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getInt(4),
                        curs.getInt(5),
                        curs.getDouble(6),
                        curs.getInt(7),
                        P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"),
                        curs.getInt(9), curs.getInt(10), curs.getInt(11),
                        curs.getInt(12), curs.getInt(13), curs.getInt(14),
                        curs.getString(15), curs.getString(16), curs.getString(17),
                        P8.ConvertStringToDate(curs.getString(18), "yyyy-MM-dd"));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return cm;
    }

    public ArrayList<HashMap<String, String>> getLoansByMember(String vono,
                                                               String memno, CMember cm) {
        ArrayList<HashMap<String, String>> itms = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        HashMap<String, String> map;
        Integer intODB;

        Log.d(TAG, "VONo: " + vono + " MemNo.: " + memno);

        if (cm != null) {
            // Add savings record
            map = new HashMap<String, String>();
            map.put("[ProductSymbol]", "");
            map.put("[LoanNo]", "Savings");
            map.put("[TargetAmtLoan]", cm.get_TargetAmtSav().toString());
            map.put("[IAB]", cm.get_SIA().toString());
            map.put("[ReceAmt]", cm.get_ReceAmt().toString());
            map.put("[StName]", "");
            map.put("[ODText]", "");
            map.put("[ODAmt]", "");

            itms.add(map);

        }

        try {
            curs = db
                    .rawQuery(
                            "SELECT L.[LoanNo], L.[TargetAmtLoan], L.[IAB], L.[ReceAmt], L.[LnStatus], "
                                    + "S.[StName], L.[ODB], L.[ProductSymbol] FROM CLoans L INNER JOIN LoanStatus S ON L.[LnStatus]=S.[LnStatus] "
                                    + "WHERE L.[OrgNo]=? AND L.[OrgMemNo]=?",
                            new String[]{vono, memno});

            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    map.put("[ProductSymbol]", curs.getString(7));
                    map.put("[LoanNo]", Integer.toString(curs.getInt(0)));
                    map.put("[TargetAmtLoan]", Integer.toString(curs.getInt(1)));
                    map.put("[IAB]", Integer.toString(curs.getInt(2)));
                    map.put("[ReceAmt]", Integer.toString(curs.getInt(3)));
                    map.put("[StName]", curs.getString(5));

                    intODB = curs.getInt(6);
                    if (intODB > 0) {
                        map.put("[ODText]", "Overdue");
                        map.put("[ODAmt]", intODB.toString());
                    }
                    /*
                     * else if(intODB < 0) { map.put("[ODText]", "Advance");
					 * intODB = -intODB; map.put("[ODAmt]", intODB.toString());
					 * }
					 */
                    else {
                        map.put("[ODText]", "");
                        map.put("[ODAmt]", "");
                    }

                    itms.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return itms;
    }

    public ArrayList<CLoan> getAllCLoansByMember(String vono, String memno) {
        ArrayList<CLoan> cls = new ArrayList<CLoan>();
        CLoan cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_CLOANS, new String[]{"[OrgNo]",
                            "[OrgMemNo]", "[ProjectCode]", "[LoanNo]", "[LoanSlNo]",
                            "[ProductNo]", "[IntrFactorLoan]", "[PrincipalAmt]",
                            "[SchmCode]", "[InstlAmtLoan]", "[DisbDate]", "[LnStatus]",
                            "[PrincipalDue]", "[InterestDue]", "[TotalDue]",
                            "[TargetAmtLoan]", "[TotalReld]", "[Overdue]",
                            "[BufferIntrAmt]", "[ReceAmt]", "[IAB]", "[ODB]", "[TRB]",
                            "[LB]", "[PDB]", "[IDB]", "[InstlPassed]",
                            "[OldInterestDue]", "[ProductSymbol]", "[LoanRealized]"},
                    "[OrgNo]=? AND [OrgMemNo]=?", new String[]{vono, memno},
                    null, null, null);
            if (curs.moveToFirst()) {
                do {
                    cl = new CLoan(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getInt(3), curs.getInt(4),
                            curs.getString(5), curs.getDouble(6),
                            curs.getInt(7), curs.getString(8), curs.getInt(9),
                            P8.ConvertStringToDate(curs.getString(10),
                                    "yyyy-MM-dd"), curs.getInt(11),
                            curs.getInt(12), curs.getInt(13), curs.getInt(14),
                            curs.getInt(15), curs.getInt(16), curs.getInt(17),
                            curs.getDouble(18), curs.getInt(19),
                            curs.getInt(20), curs.getInt(21), curs.getInt(22),
                            curs.getInt(23), curs.getInt(24), curs.getInt(25),
                            curs.getInt(26), curs.getDouble(27),
                            curs.getString(28), curs.getInt(29));
                    cls.add(cl);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cls;
    }

    public CLoan getCloan(Integer ln) {
        CLoan cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_CLOANS, new String[]{"[OrgNo]",
                            "[OrgMemNo]", "[ProjectCode]", "[LoanNo]", "[LoanSlNo]",
                            "[ProductNo]", "[IntrFactorLoan]", "[PrincipalAmt]",
                            "[SchmCode]", "[InstlAmtLoan]", "[DisbDate]", "[LnStatus]",
                            "[PrincipalDue]", "[InterestDue]", "[TotalDue]",
                            "[TargetAmtLoan]", "[TotalReld]", "[Overdue]",
                            "[BufferIntrAmt]", "[ReceAmt]", "[IAB]", "[ODB]", "[TRB]",
                            "[LB]", "[PDB]", "[IDB]", "[InstlPassed]",
                            "[OldInterestDue]", "[ProductSymbol]", "[LoanRealized]"},
                    "[LoanNo]=?", new String[]{ln.toString()}, null, null,
                    null);
            if (curs.moveToFirst()) {
                cl = new CLoan(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getInt(3),
                        curs.getInt(4),
                        curs.getString(5),
                        curs.getDouble(6),
                        curs.getInt(7),
                        curs.getString(8),
                        curs.getInt(9),
                        P8.ConvertStringToDate(curs.getString(10), "yyyy-MM-dd"),
                        curs.getInt(11), curs.getInt(12), curs.getInt(13), curs
                        .getInt(14), curs.getInt(15), curs.getInt(16),
                        curs.getInt(17), curs.getDouble(18), curs.getInt(19),
                        curs.getInt(20), curs.getInt(21), curs.getInt(22), curs
                        .getInt(23), curs.getInt(24), curs.getInt(25),
                        curs.getInt(26), curs.getDouble(27),
                        curs.getString(28), curs.getInt(29));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return cl;
    }

    // Get Overdue list
    public ArrayList<HashMap<String, String>> getOverDueByVO(String vono) {
        ArrayList<HashMap<String, String>> memWiseLoanList = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;

        String selectQuery = "SELECT L.[OrgMemNo], M.[MemberName], L.[PrincipalAmt], L.[DisbDate], "
                + "CASE WHEN L.[LnStatus] >= 2 THEN L.[LB] ELSE L.[ODB] END AS OD, L.[LnStatus], S.[StName] "
                + "FROM "
                + DBHelper.TBL_CLOANS
                + " L INNER JOIN "
                + DBHelper.TBL_CMEMBERS
                + " M ON "
                + "L.[OrgNo]=M.[OrgNo] AND L.[OrgMemNo]=M.[OrgMemNo] INNER JOIN "
                + DBHelper.TBL_LOANSTATUS
                + " S ON L.[LnStatus]=S.[LnStatus] "
                + "WHERE (L.[ODB] > 0 OR L.[LnStatus] >= 2) AND L.[OrgNo]=? "
                + "ORDER BY L.[LnStatus], L.[OrgMemNo]";
        try {
            curs = db.rawQuery(selectQuery, new String[]{vono});

            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                int lns = -100;
                do {

                    if (lns != curs.getInt(5)) {
                        // New group header
                        lns = curs.getInt(5);
                        map = new HashMap<String, String>();
                        map.put("GPHead", curs.getString(6));
                        map.put("MemNo", "");
                        map.put("MemberName", "");
                        map.put("DisbAmt", "");
                        map.put("DisbDate", "");
                        map.put("Overdue", "");

                        memWiseLoanList.add(map);
                    }
                    map = new HashMap<String, String>();
                    map.put("GPHead", "");
                    map.put("MemNo", curs.getString(0));
                    map.put("MemberName", curs.getString(1));
                    map.put("DisbAmt", Integer.toString(curs.getInt(2)));
                    map.put("DisbDate", P8.FormatDate(
                            P8.ConvertStringToDate(curs.getString(3)),
                            "dd-MMM-yyyy"));
                    map.put("Overdue", Integer.toString(curs.getInt(4)));

                    memWiseLoanList.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return memWiseLoanList;
    }

    public void addTransaction(CLoan cl, String TType, Integer amt)
            throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();

            Log.d(TAG,
                    "Loan(" + TType + ") VONo: " + cl.get_OrgNo() + " MemNo: "
                            + cl.get_OrgMemNo() + " Amount: " + amt.toString());

            cl.Repay(amt);

            InsertTrans(cl.get_OrgNo(), cl.get_OrgMemNo(),
                    cl.get_ProjectCode(), cl.get_LoanNo(), amt, TType, cl.get_ProductSymbol(), cl.get_TRB(), 0,
                    DBHelper.TBL_TRANSACT);

            cv.clear();
            cv.put("[ReceAmt]", cl.get_ReceAmt());
            cv.put("[IAB]", cl.get_IAB());
            cv.put("[ODB]", cl.get_ODB());
            cv.put("[TRB]", cl.get_TRB());
            cv.put("[LB]", cl.get_LB());
            cv.put("[PDB]", cl.get_PDB());
            cv.put("[IDB]", cl.get_IDB());

            db.update(DBHelper.TBL_CLOANS, cv,
                    "[OrgNo]=? AND [OrgMemNo]=? AND [LoanNo]=?", new String[]{
                            cl.get_OrgNo(), cl.get_OrgMemNo(),
                            cl.get_LoanNo().toString()});

            PO po = getPO();
            UpdatePOCash(po.get_CashInHand() + amt);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public void addTransaction(CMember cm, String TType, Integer amt)
            throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();

            Log.d(TAG,
                    "Savings(" + TType + ") VONo: " + cm.get_OrgNo()
                            + " MemNo: " + cm.get_OrgMemNo() + " Amount: "
                            + amt.toString());

            cm.Save(amt);

            InsertTrans(cm.get_OrgNo(), cm.get_OrgMemNo(),
                    cm.get_ProjectCode(), null, amt, TType, "", cm.get_SB(), 0,
                    DBHelper.TBL_TRANSACT);

            cv.clear();
            cv.put("[ReceAmt]", cm.get_ReceAmt());
            cv.put("[SB]", cm.get_SB());
            cv.put("[SIA]", cm.get_SIA());

            db.update(DBHelper.TBL_CMEMBERS, cv, "[OrgNo]=? AND [OrgMemNo]=?",
                    new String[]{cm.get_OrgNo(), cm.get_OrgMemNo()});

            PO po = getPO();
            UpdatePOCash(po.get_CashInHand() + amt);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    private void UpdatePOCash(Integer amt) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[CashInHand]", amt);
        db.update(DBHelper.TBL_PO, cv, null, null);
    }

    private void InsertTrans(String OrgNo, String OrgMemNo, String ProjectCode,
                             Integer LoanNo, Integer amt, String ColcFor, String psym, Integer bal, Integer smsstat, String tblName) {
        ContentValues cv = new ContentValues();

        if (tblName.equalsIgnoreCase(DBHelper.TBL_TRANSACT))
            cv.put("[ColcID]", 0);
        cv.put("[OrgNo]", OrgNo);
        cv.put("[OrgMemNo]", OrgMemNo);
        cv.put("[ProjectCode]", ProjectCode);
        cv.put("[LoanNo]", LoanNo);
        cv.put("[ColcAmt]", amt);
        cv.put("[ColcFor]", ColcFor);
        cv.put("[ProductSymbol]", psym);
        cv.put("[Balance]", bal);
        cv.put("[SMSStatus]", smsstat);
        db.insert(tblName, null, cv);
        // db.insert(DBHelper.TBL_TRANSACT, null, cv);
    }

    public void AssignCollector(String collno, String POName) throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();
            cv.put("CONo", collno);
            cv.put("COName", POName);
            cv.put("EnteredBy", collno);
            cv.put("AltCOName", POName);
            cv.put("EMethod", 0);
            cv.put("CashInHand", 0);
            cv.put("SessionNo", 0);
            cv.put("Password", "");
            cv.put("BranchCode", "0000");
            cv.put("BranchName", "");

            db.update(DBHelper.TBL_PO, cv, null, null);
            db.delete(DBHelper.TBL_VOLIST, null, null);
            db.delete(DBHelper.TBL_LOAN_PRODUCTS, null, null);
            db.delete(DBHelper.TBL_CMEMBERS, null, null);
            db.delete(DBHelper.TBL_CLOANS, null, null);
            db.delete(DBHelper.TBL_TRANSACT, null, null);

            db.setTransactionSuccessful();

        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } catch (Exception e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public void UpdateSessionData(String fileName) throws SQLException,
            IOException {
        try {
            db.beginTransaction();
            executeSQLScript(fileName);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void ClearTransactions() throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();
            cv.put("CashInHand", 0);

            db.update(DBHelper.TBL_PO, cv, null, null);
            db.delete(DBHelper.TBL_TRANSACT, null, null);
            db.setTransactionSuccessful();

        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } catch (Exception e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<Transact> getTransactions(String vono, String memno, Integer smsstatus) {
        Transact ct = null;
        Cursor curs = null;

        ArrayList<Transact> ret = new ArrayList<Transact>();

        try {
            curs = db.query(DBHelper.TBL_TRANSACT, new String[]{
                            "_id", "[ColcID]", "[OrgNo]", "[OrgMemNo]", "[ProjectCode]",
                            "[LoanNo]", "[ColcAmt]", "[SavAdj]", "[ColcDate]", "[ColcFor]",
                            "[ProductSymbol]", "[Balance]", "[SMSStatus]"},
                    "[OrgNo]=? AND [OrgMemNo]=? AND [SMSStatus]=?", new String[]{vono, memno, smsstatus.toString()},
                    null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    ct = new Transact(curs.getInt(0), curs.getInt(1),
                            curs.getString(2), curs.getString(3), curs.getString(4),
                            curs.getInt(5), curs.getInt(6), curs.getInt(7),
                            P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"),
                            curs.getString(9), curs.getString(10), curs.getInt(11),
                            curs.getInt(12));

                    ret.add(ct);
                } while (curs.moveToNext());

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public void ConfirmSMSSent(String recs) {
        ContentValues cv = new ContentValues();

        try {
            cv.put("[SMSStatus]", 1);
            db.update(DBHelper.TBL_TRANSACT, cv, "_id IN (" + recs + ")", null);

        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }


    public boolean HasTransactions() {
        int cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                + DBHelper.TBL_TRANSACT, null);
        return (cnt > 0);
    }

    public String PrepareDataForSending(String po) throws IOException {
        File file = new File(App.getContext().getCacheDir(), "data" + po.trim());
        String strExportFile = file.getPath();
        if (file.exists())
            file.delete();

        BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(file));
        bos.write("-- Data from BM device".getBytes());
        ExportTableAsSQL(
                bos,
                "SELECT T.OrgNo, T.OrgMemNo, T.ProjectCode, T.LoanNo, T.ColcAmt, T.SavAdj, "
                        + "Date(T.ColcDate), T.ColcFor, T.ColcDate as ModifyTime, V.CONo, C.EnteredBy, SessionNo, "
                        + "T.ProductSymbol, T.Balance, T.SMSStatus "
                        + "FROM Transact T INNER JOIN VOList V ON T.OrgNo=V.OrgNo, POList C",
                new String[]{"S", "S", "S", "I", "I", "I", "S", "S", "S",
                        "I", "S", "I", "S", "I", "I"},
                "INSERT INTO Transact(OrgNo, OrgMemNo, ProjectCode, LoanNo, ColcAmt, SavAdj, ColcDate, ColcFor, "
                        + "ModifyTime, CONo, EnteredBy, SessionNo, ProductSymbol, Balance, SMSStatus)");

        bos.close();

        return strExportFile;
    }

    private void ExportTableAsSQL(BufferedOutputStream bos, String selSql,
                                  String[] colTyes, String targetSql) throws IOException {
        Cursor cur = db.rawQuery(selSql, new String[0]);
        Log.d(TAG,
                "Selection SQL: " + selSql + "\nRcords found: "
                        + Integer.toString(cur.getCount()) + "\nTarge SQL: "
                        + targetSql);

        int numcols = cur.getColumnCount();
        cur.moveToFirst();

        while (cur.getPosition() < cur.getCount()) {
            String vls = "";
            for (int idx = 0; idx < numcols; idx++) {
                if (vls.length() > 0)
                    vls += ", ";
                if (colTyes[idx].equals("S")) {
                    vls += ("'" + cur.getString(idx) + "'");
                } else {
                    vls += Integer.toString(cur.getInt(idx));
                }
            }
            bos.write(("\r\n" + targetSql + " VALUES (" + vls + ");")
                    .getBytes());
            cur.moveToNext();
        }

        cur.close();
    }

    public void SetStatusEntryLocked() throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();
            cv.put("EMETHOD", 0);
            db.update(DBHelper.TBL_PO, cv, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.toString(), e);
            throw e;
        } finally {
            db.endTransaction();
        }
    }

    public void executeSQLScript(String dbname) throws IOException,
            SQLException {
        FileInputStream is;
        BufferedReader reader;
        StringBuilder sb = new StringBuilder();
        String stm;
        Integer i;

        is = new FileInputStream(dbname);
        reader = new BufferedReader(new InputStreamReader(is));
        try {
            String line = reader.readLine();
            while (line != null) {
                // Log.d(TAG, line);
                stm = line.trim();
                if (stm.length() > 0) {
                    if (!stm.startsWith("--")) {
                        sb.append(stm);
                        i = sb.indexOf(";");
                        if (i != -1) {
                            // Log.d(TAG, sb.substring(0, i+1));
                            db.execSQL(sb.substring(0, i + 1));
                            sb.delete(0, i + 1);
                        }
                    }
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            // TODO Handle Script Failed to Load
            Log.e(TAG, e.toString(), e);
            throw e;
        } catch (SQLException e) {
            // TODO Handle Script Failed to Execute
            Log.e(TAG, e.toString(), e);
            throw e;
        } finally {
            reader.close();
            is.close();
        }
    }
}
