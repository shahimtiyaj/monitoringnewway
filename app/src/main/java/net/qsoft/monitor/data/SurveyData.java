package net.qsoft.monitor.data;

import net.qsoft.monitor.P8;

import java.util.Date;

/**
 * Created by zferdaus on 9/24/2017.
 */

public class SurveyData {
    Integer Id=0;
    Integer EventId=0;
    Integer  SectionId=0;
    String OrgNo="";
    String OrgMemNo="";
    Integer Question=0;
    Integer Answer=0;
    Integer Score=0;
    String Status="N";
    Date Updated_At;
    String SubSectionId="";
    String Remarks="";
    Integer MonitorNo = 0;

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }

    public double getLati() {
        return lati;
    }

    public void setLati(double lati) {
        this.lati = lati;
    }

    double longi;
    double lati;


    public SurveyData() {

    }

    public Integer getId() {
        if(Id==null)
            return 0;
        else
            return Id;
    }

    public Integer getMonitorNo() {
        return MonitorNo;
    }

    public void setMonitorNo(Integer monitorNo) {
        MonitorNo = monitorNo;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getEventId() {
        return EventId;
    }

    public void setEventId(Integer eventId) {
        EventId = eventId;
    }

    public Integer getSectionId() {
        return SectionId;
    }

    public void setSectionId(Integer sectionId) {
        SectionId = sectionId;
    }

    public String getSubSectionId() {
        return SubSectionId;
    }

    public void setSubSectionId(String subSectionId) {
        SubSectionId = subSectionId;
    }

    public String getOrgNo() {
        return OrgNo;
    }

    public void setOrgNo(String orgNo) {
        OrgNo = orgNo;
    }

    public String getOrgMemNo() {
        return OrgMemNo;
    }

    public void setOrgMemNo(String orgMemNo) {
        OrgMemNo = orgMemNo;
    }

    public Integer getQuestion() {
        return Question;
    }

    public void setQuestion(Integer question) {
        Question = question;
    }

    public Integer getAnswer() {
        return Answer;
    }

    public void setAnswer(Integer answer) {
        Answer = answer;
    }

    public Integer getScore() {
        return Score;
    }

    public void setScore(Integer score) {
        Score = score;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public Date getUpdated_At() {
        return Updated_At;
    }

    public void setUpdated_At(Date updated_At) {
        Updated_At = updated_At;
    }

    public void setUpdated_At(String _Updated_At) {
        Updated_At = P8.ConvertStringToDate(_Updated_At, "yyyy-MM-dd hh:mm:ss");
    }
}
