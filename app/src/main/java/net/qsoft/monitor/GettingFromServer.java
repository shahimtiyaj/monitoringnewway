package net.qsoft.monitor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import net.qsoft.monitor.NetworkService.ErrorDialog;
import net.qsoft.monitor.NetworkService.VolleyCustomRequest;
import net.qsoft.monitor.data.ActiveUser;
import net.qsoft.monitor.data.DAO;
import net.qsoft.monitor.data.GetData;
import net.qsoft.monitor.data.Respondent;
import net.qsoft.monitor.data.SurveyData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static android.util.Log.d;
import static net.qsoft.monitor.data.DBHelper.TBL_RESPONDENTS;
import static net.qsoft.monitor.data.DBHelper.TBL_SURVEYDATA;

/**
 * Created by QSPA10 on 3/20/2018.
 */

public class GettingFromServer extends AppCompatActivity {

    private static final String TAG = GettingFromServer.class.getSimpleName();

    private static final String hit_url = "http://103.43.93.162/mnw/download";

    String evenRoll, token, time;
    int evenId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.monitor_select_layout);

//        GetData getData = GetData.getInstance();
//        getData.dataGetFromServer(getApplicationContext());

        getServerData();

    }


    public void getServerData() {

        ActiveUser usr = ActiveUser.getInstance();
        token = usr.getToken();
        evenId = usr.getEventId();
        HashMap<String, String> params = new HashMap<>();
        params.put("token", token);
        params.put("eventid", String.valueOf(evenId));
      //  params.put("lastDownloadTime", LastDownloadDateTime());

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hit_url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            Log.d("response", "Size: " + response);

                            if (status.equals("success")) {


                                JSONObject jObj = new JSONObject("data");
                                JSONArray getSurveyData = jObj.getJSONArray("servey_data");

                                for (int i = 0; i < getSurveyData.length(); i++) {

                                    JSONObject c = getSurveyData.getJSONObject(i);

//                                    SurveyData surveyData = new SurveyData();
//
//                                    //  surveyData.setId(c.getInt("id"));
//                                    surveyData.setEventId(c.getInt("eventid"));
//                                    c.getString("sec_no");
//                                    surveyData.setAnswer(c.getInt("answer"));
//                                    surveyData.setScore(c.getInt("score"));
//                                    c.getString("time");
//                                    surveyData.setOrgNo(c.getString("orgno"));
//                                    surveyData.setOrgMemNo(c.getString("orgmemno"));
//                                    surveyData.setSubSectionId(c.getString("sub_sec_id"));
//                                    surveyData.setMonitorNo(c.getInt("monitorno"));
//                                    surveyData.setRemarks(c.getString("remarks"));
//                                    surveyData.setLongi(c.getDouble("longi"));
//                                    surveyData.setLati(c.getDouble("lati"));

                                    DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_SURVEYDATA + "(EventId, SectionId, SubSectionId, " +
                                                    "OrgNo, OrgMemNo, Question, " + "Answer, Score, Remarks," +
                                                    " MonitorNo, Status, Updated_At, Longi, Lati) " +
                                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                            new String[]{c.getString("eventid"), c.getString("sec_no"),
                                                    c.getString("answer"), c.getString("score"),
                                                    c.getString("time"), c.getString("orgno"),
                                                    c.getString("orgmemno"), c.getString("sub_sec_id"),
                                                    c.getString("monitorno"), c.getString("remarks"),
                                                    c.getString("longi"), c.getString("lati")});

                                    Toast.makeText(getApplicationContext(), "Survey Data Loding...", Toast.LENGTH_LONG).show();

                                }

                                JSONArray getResponDatnt = jObj.getJSONArray("respondents");

                                for (int i = 0; i < getResponDatnt.length(); i++) {

                                    JSONObject c = getResponDatnt.getJSONObject(i);

//                                    Respondent respondentData = new Respondent();
//
//                                    respondentData.setId(c.getInt("id"));
//                                    c.getString("branchcode");
//                                    respondentData.setOrgNo(c.getString("orgno"));
//                                    respondentData.setEventId(c.getInt("event_id"));
//                                    c.getString("sec_no");
//                                    respondentData.setOrgMemNo(c.getString("orgmemno"));
//                                    respondentData.setAdmissionDate(c.getString("admission_date"));
//                                    respondentData.setLoanNo(c.getInt("loanno"));
//                                    respondentData.setDisbDate(c.getString("disbdate"));
//                                    respondentData.setAmount(c.getInt("amnt"));
//                                    c.getString("time");
//                                    respondentData.setMemberName(c.getString("MemberName"));
//                                    respondentData.setCONo(c.getString("cono"));
//                                    respondentData.setCOName(c.getString("coname"));
//                                    respondentData.setMonitorNo(c.getInt("MonitorNo"));
//                                    respondentData.setSubSectionId(c.getString("sub_sec_id"));


                                    DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_RESPONDENTS + "(Id, EventId, SectionId, " +
                                                    "SubSectionId, OrgNo, OrgMemNo, MemberName, AdmissionDate, LoanNo," +
                                                    " DisbDate, Amount, CONo, COName, MonitorNo, Status, Updated_At) " +
                                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                                            new String[]{c.getString("id"), c.getString("branchcode"),
                                                    c.getString("orgno"), c.getString("event_id"),
                                                    c.getString("sec_no"), c.getString("orgmemno"),
                                                    c.getString("orgmemno"), c.getString("sub_sec_id"),
                                                    c.getString("admission_date"), c.getString("loanno"),
                                                    c.getString("disbdate"), c.getString("amnt"),
                                                    c.getString("time"), c.getString("MemberName"),
                                                    c.getString("cono"), c.getString("coname"),
                                                    c.getString("MonitorNo"), c.getString("sub_sec_id")});
                                    Toast.makeText(getApplicationContext(), "Respondant Data Loding...", Toast.LENGTH_LONG).show();

                                }


                            } else {
                                Toast.makeText(getApplicationContext(), "Try again later!", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "Try again later!", Toast.LENGTH_LONG).show();
                        }

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // TODO Auto-generated method stub
                        String message = null;
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof NoConnectionError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            message = volleyError.toString();
                        }
                        //    P8.ShowError(getApplicationContext(), message);
                    }
                });

        CloudRequest.getInstance(getApplicationContext()).addToRequestQueue(postRequest);
    }

    public String LastDownloadDateTime() {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();
        return dt.format(date);
        // return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM).format(date);
        // return DateFormat.getDateTimeInstance().format(new Date());//latest and standard formt
    }

}
