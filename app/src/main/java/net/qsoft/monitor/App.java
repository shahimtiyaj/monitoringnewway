package net.qsoft.monitor;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import java.util.Date;

public class App extends Application{
	private static final String TAG = App.class.getSimpleName();
    private static Context mContext;
    private static Date datBranchOPenDate=null;
    private static Date datLastSyncDate=null;
	private static int versionCode=0;
	private static String versionName="";
	private static Activity mSMSActivity=null;
	private static double tvLongi = 0;
	private static double tvLati = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

		try {
			PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			Log.d(TAG, e.getMessage());
		}
    }

    public static Context getContext(){
        return mContext;
    }

	public  static final String getBaseURL () {
		return "http://103.43.93.162/mnw";
	}
    public static final Integer getVersionCode() { return versionCode; }
    public static final String getVersionName() { return versionName; }

	public  static final void setSMSActivity(Activity x) { mSMSActivity = x; }
	public  static final Activity getmSMSActivity() { return  mSMSActivity; }

	/**
	 * @return the datBranchOPenDate
	 */
	public static final Date getBranchOPenDate() {
		return datBranchOPenDate;
	}

	/**
	 * @param datBranchOPenDate the datBranchOPenDate to set
	 */
	public static final void setBranchOPenDate(Date datBranchOPenDate) {
		App.datBranchOPenDate = datBranchOPenDate;
	}

	/**
	 * @return the datLastSyncDate
	 */
	public static final Date getLastSyncDate() {
		return datLastSyncDate;
	}

	/**
	 * @param datLastSyncDate the datLastSyncDate to set
	 */
	public static final void setLastSyncDate(Date datLastSyncDate) {
		App.datLastSyncDate = datLastSyncDate;
	}
	
	public static final String getLastSyncGaps() {
		String ret="";
		Date c = new Date();
		if(getLastSyncDate() != null) {
			if(getLastSyncDate().after(P8.ConvertStringToDate("2000-01-01", "yyyy-MM-dd"))) {
				long different = c.getTime() - getLastSyncDate().getTime();
				
				long secondsInMilli = 1000;
		        long minutesInMilli = secondsInMilli * 60;
		        long hoursInMilli = minutesInMilli * 60;
		        long daysInMilli = hoursInMilli * 24;

		        long elapsedDays = different / daysInMilli;
		        different = different % daysInMilli;

		        long elapsedHours = different / hoursInMilli;
		        different = different % hoursInMilli;

		        long elapsedMinutes = different / minutesInMilli;
		        different = different % minutesInMilli;

		        long elapsedSeconds = different / secondsInMilli;

		        
		        ret = String.format("Last download since\n %d days, %d hours, %d minutes", 
		        		elapsedDays, elapsedHours, elapsedMinutes);	//, %d seconds%n, , elapsedSeconds
			}
		}
		
		return ret;	
	}

	public static final void setLocation(double longi, double lati) {
		tvLongi = longi;
		tvLati = lati;
	}

	public static double getLongitude() {
		return tvLongi;
	}

	public static double getLatitude() {
		return tvLati;
	}
}
